EU = RegisterMod("Eternal Return", 1)

EU.game = Game()
EU.sfx = SFXManager()
EU.rng = RNG()
EU.itempool = EU.game:GetItemPool()
EU.itemconfig = Isaac.GetItemConfig()

EU.sd = {}

--local deal,angel,devil,isduality=DEALAPI:GetChance()

local modfolder = "eternal-update-repentance"
local json = require("json")

EU:AddCallback(ModCallbacks.MC_PRE_GAME_EXIT, function(_)
	EU.SaveData(EU, json.encode(EU.sd))
end)

local achgfxpath = "gfx/ui/achievement/"

include("resources/scripts/eternalAPI/script")
include("resources/scripts/dealapi/script")
--require("dealapi_code")

do -- special data --
	function EU.GetStageName()
		local level = EU.game:GetLevel()
		local stagenum = level:GetStage()
		local stagename = level:GetName()
		if stagenum < 9 then
			if level:GetStage() % 2 ~= 1 or (level:GetCurses() & LevelCurse.CURSE_OF_LABYRINTH > 0) then
				return string.sub(stagename, 0, -4)
			else
				return string.sub(stagename, 0, -3)
			end
		else
			return stagename
		end
	end
	
	--[[EU.themes = {
		white = 0,
		normal = 1,
		rbw = 2,
		gay = 3,
		shit = 4
	}]]
	
	function EU.split(array, size)
		local t = {}
		local n = 1
		for i = 1, #array do
			if i - 1 % size == 0 then
				for v = 1, size do
					t[n][v] = array[i + v]
				end
				n = n + 1
			end
		end
		return t
	end
	
	function EU.len(t)
		local count = 0
		for _ in pairs(t) do count = count + 1 end
		return count
	end
	
	function EU.lowest(t)
		local a = math.huge
		for _, n in ipairs(t) do
			if type(n) == 'number' then 
				a = math.min(a, n)
			end
		end
		if a == math.huge then return end
		return a
	end
	
	function EU.highest(t)
		local a = -math.huge
		for _, n in ipairs(t) do
			if type(n) == 'number' then 
				a = math.max(a, n)
			end
		end
		if a == -math.huge then return end
		return a 
	end
	
	function EU.dupetear(t)
		local nt = Isaac.Spawn(t.Type, t.Variant, t.SubType, t.Position, t.Velocity, t):ToTear()
		nt.Parent = t
		nt.Color = t.Color
		nt.FallingSpeed = t.FallingSpeed
		nt.FallingAcceleration = t.FallingAcceleration
		nt.Height = t.Height
		nt.Scale = t.Scale
		nt.CollisionDamage = t.CollisionDamage
		nt.TearFlags = nt.TearFlags | t.TearFlags
		return nt
	end
	
	function EU.isSinglePlayer()
		local t = {}
		if t.val == nil then t.val = {} end
		for i = 0, EU.game:GetNumPlayers() - 1 do
			local p = Isaac.GetPlayer(i)
			if not t.val[p.ControllerIndex] then t.val[p.ControllerIndex] = true end
			if i == EU.game:GetNumPlayers() - 1 then
				local nump = EU.len(t.val)
				if nump > 1 then
					return false
				else
					return true
				end
				break
			end
		end
	end
	
	function EU.getClosestEntity(pos, TYPE, VARIANT, SUBTYPE) -- caps are optional
		local t_dist = {}
		local t = {}
		if TYPE and VARIANT and SUBTYPE then
			t = Isaac.FindByType(TYPE, VARIANT, SUBTYPE, false, false)
		else
			t = Isaac.GetRoomEntities()
		end
		if t ~= nil and EU.len(t) ~= 0 then
			for i, e in pairs(t) do
				t_dist[i] = (pos - e.Position):Length()
				local lowest = EU.lowest(t_dist)
				if t_dist[i] and t_dist[i] == lowest then
					return e
				end
			end
		end
	end
	
	function EU.getFurthestEntity(pos, TYPE, VARIANT, SUBTYPE) -- caps are optional
		local t_dist = {}
		local t = {}
		if TYPE and VARIANT and SUBTYPE then
			t = Isaac.FindByType(TYPE, VARIANT, SUBTYPE, false, false)
		else
			t = Isaac.GetRoomEntities()
		end
		if t ~= nil and EU.len(t) ~= 0 then
			for i, e in pairs(t) do
				t_dist[i] = (pos - e.Position):Length()
				local highest = EU.highest(t_dist)
				if t_dist[i] and t_dist[i] == highest then
					return e
				end
			end
		end
	end
	
	function EU.getPlayerBySlot(num)
		for i = 0, EU.game:GetNumPlayers() - 1 do
			local p = Isaac.GetPlayer(i)
			local d = p:GetData()
			if d.EU == nil then d.EU = {} end
			if d.EU.playerSlot and d.EU.playerSlot == num then
				if p then
					return p
				else
					return Isaac.GetPlayer()
				end
			end
		end
	end
	
	function EU.theme(theme, player) -- use in debug console
		for i, e in pairs(Isaac.FindByType(EntityType.ENTITY_PLAYER, -1, -1, false, false)) do; local p = e:ToPlayer();
			local d = p:GetData()
			if d.EU.playerSlot and d.EU.playerSlot == player then
				if type(theme) == "number" then
					d.EU.hudtheme = theme
				elseif type(theme) == "string" then
					for i = 0, EU.len(EU.hudthemes) do
						if theme == EU.hudthemes[i].name then
							d.EU.hudtheme = i
							break
						end
					end
				end
			end
		end
	end
	
	function EU.name(name, player) -- use in debug console
		for i, e in pairs(Isaac.FindByType(EntityType.ENTITY_PLAYER, -1, -1, false, false)) do; local p = e:ToPlayer();
			local d = p:GetData()
			if d.EU and d.EU.playerSlot and d.EU.playerSlot == player then
				d.EU.name = name
			end
		end
	end
	
	function EU.addplayer(p, ptype, cidx, isfake, updhud)
		if not ptype then ptype = 0 end
		if not p then p = Isaac.GetPlayer() end
		local lastpidx = EU.game:GetNumPlayers() - 1
		
		if lastpidx >= 63 then return nil else
			Isaac.ExecuteCommand('addplayer ' .. ptype .. ' ' .. cidx)
			local _p = Isaac.GetPlayer(lastpidx + 1)
			local d = _p:GetData()
			if isfake then
				_p.Parent = p
				if updhud and updhud == true then
					EU.game:GetHUD():AssignPlayerHUDs()
				end
			end
			return _p
		end
	end
	
	function EU.SpawnChain(parent, child, beads)
		local firstbead=Isaac.Spawn(1000, 37916, 0, parent.Position + parent.Velocity, Vector(0,0), parent):ToEffect()
		firstbead:GetData().NumOfElements=beads
		firstbead:GetData().BeadNum = 1
		firstbead.Parent = parent
		firstbead.Child = child
		local bead = nil
		for i = 1,beads-1 do
			bead=Isaac.Spawn(1000, 37916, 0, parent.Position + parent.Velocity, Vector(0,0), parent):ToEffect()
			bead.Parent=parent
			bead:GetData().NumOfElements=beads
			bead:GetData().BeadNum=i+1
			bead.Child=child
		end
		return firstbead
	end
	
	function EU:getController(p)
		local cidx = p.ControllerIndex
		local pastcoins = p:GetNumCoins()
		local fpid = Game():GetNumPlayers()
		local strP = string.gmatch(Isaac.ExecuteCommand('addplayer '.. PlayerType.PLAYER_THELOST ..' '..cidx),'%S+')
		local w = {}
		for z in strP do
			table.insert(w, z)
		end
		local fp = Isaac.GetPlayer(fpid)
		fp:GetSprite():Load('gfx/player_quickdeath.anm2', false)
		fp.SpriteScale = Vector(0,0)
		fp.ControlsEnabled = false
		fp.Visible = false
		fp.Parent = p
		Game():GetHUD():AssignPlayerHUDs()
		p:AddCoins(-(p:GetNumCoins() - pastcoins))
		fp:Die()
		return w[4]
	end
	
	function EU.p(name, id, theme)
		EU.name(name, id)
		if theme == nil then theme = 0 end
		EU.theme(theme, id)
		if type(theme) == "number" then
			---print("player id", id, "assigned with name", '"' .. name .. '"', "and theme", '"' .. EU.hudthemes[theme].name .. '".')
		else
			---print("player id", id, "assigned with name", '"' .. name .. '"', "and theme", '"' .. theme .. '".')
		end
	end
	
	do -- screen functions
	
		function EU.getScreenCenter()
			local room = Game():GetRoom()
			local shape = room:GetRoomShape()
			local centerOffset = room:GetCenterPos() - room:GetTopLeftPos()
			
			local pos = room:GetCenterPos()
			
			if centerOffset.X > 260 then
				pos.X = pos.X - 260
			end
			if shape == RoomShape.ROOMSHAPE_LBL or shape == RoomShape.ROOMSHAPE_LTL then
				pos.X = pos.X - 260
			end
			if centerOffset.Y > 140 then
				pos.Y = pos.Y - 140
			end
			if shape == RoomShape.ROOMSHAPE_LTR or shape == RoomShape.ROOMSHAPE_LTL then
				pos.Y = pos.Y - 140
			end
			
			return Isaac.WorldToRenderPosition(pos, false)
		end
		
		function EU.getScreenBR()
			local pos = EU.getScreenCenter() * 2
			return pos
		end
		
		function EU.getScreenBL()
			local pos = Vector(0, EU.getScreenBR(0).Y)
			return pos
		end

		function EU.getScreenTR()
			local pos = Vector(EU.getScreenBR(0).X, 0)
			return pos
		end
		
		function EU.getScreenTL()
			local pos = Vector(0, 0)
			return pos
		end
	
	end
	
	function EU.forceDoorTypes()
		local room = EU.game:GetRoom()
		for i = 0, (room:GetGridSize()) do
			local gent = room:GetGridEntity(i)
			if room:GetGridEntity(i) then
				if (gent.Desc.Type == 16) then
					local Door = gent:ToDoor()
					local s = Door:GetSprite()
					if s:GetFilename() == "gfx/grid/Door_01_NormalDoor.anm2" then
						local roomTypes = ( 
							Door:IsRoomType(2) or 
							Door:IsRoomType(3) or 
							Door:IsRoomType(4) or 
							Door:IsRoomType(5) or 
							Door:IsRoomType(7) or 
							Door:IsRoomType(8) or 
							Door:IsRoomType(10) or 
							Door:IsRoomType(11) or 
							Door:IsRoomType(12) or 
							Door:IsRoomType(13) or 
							Door:IsRoomType(14) or 
							Door:IsRoomType(15) or 
							Door:IsRoomType(16) or
							Door:IsRoomType(17) or
							Door:IsRoomType(20) or
							Door:IsRoomType(21) or
							Door:IsRoomType(23) or
							Door:IsRoomType(24) or
							Door:IsRoomType(25) or
							Door:IsRoomType(26) or
							Door:IsRoomType(29) or
							Door:IsRoomType(30)
						)
						if EU.doorsuffix[room:GetBackdropType()] and EU.doorsuffix[room:GetBackdropType()].png and EU.doorsuffix[room:GetBackdropType()].anm2 and not roomTypes and s:GetFilename() == "gfx/grid/Door_01_NormalDoor.anm2" then
							s:Load("gfx/grid/stagedoors/door_" .. EU.doorsuffix[room:GetBackdropType()].anm2 .. ".anm2", true)
							for i = 0, 3 do
								s:ReplaceSpritesheet(i, "gfx/grid/stagedoors/door_" .. EU.doorsuffix[room:GetBackdropType()].png .. ".png")
							end
							s:LoadGraphics()
							s:Update()
							if not Door:IsLocked() then
								if room:IsClear() then
									s:Play("Open", true)
								else
									s:Play("Close", true)
								end
							else
								s:Play("KeyClosed", true)
							end
						end
						if Door:IsRoomType(2)  then
							s:Load("gfx/grid/door_00_shopdoor.anm2", true)
							s:Update()
							if not Door:IsLocked() then
								if room:IsClear() then
									s:Play("Open", true)
								else
									s:Play("Close", true)
								end
							else
								s:Play("KeyClosed", true)
							end
						elseif Door:IsRoomType(12) then
							s:Load("gfx/grid/door_13_librarydoor.anm2", true)
							s:Update()
							if not Door:IsLocked() then
								if room:IsClear() then
									s:Play("Open", true)
								else
									s:Play("Close", true)
								end
							else
								s:Play("KeyClosed", true)
							end
						elseif Door:IsRoomType(13) then
							s:Load("gfx/grid/door_00_sacrificeroomdoor.anm2", true)
							s:Update()
							if not Door:IsLocked() then
								if room:IsClear() then
									s:Play("Open", true)
								else
									s:Play("Close", true)
								end
							else
								s:Play("KeyClosed", true)
							end
						elseif Door:IsRoomType(20) then
							s:Load("gfx/grid/door_02b_chestroomdoor.anm2", true)
							s:Update()
							if not Door:IsLocked() then
								if room:IsClear() then
									s:Play("Open", true)
								else
									s:Play("Close", true)
								end
							else
								s:Play("KeyClosed", true)
							end
						elseif Door:IsRoomType(21) then
							s:Load("gfx/grid/door_00_diceroomdoor.anm2", true)
							s:Update()
							if not Door:IsLocked() then
								if room:IsClear() then
									s:Play("Open", true)
								else
									s:Play("Close", true)
								end
							else
								s:Play("KeyClosed", true)
							end
						end
					end
				end
			end
		end
	end
	
	function EU.renderGlassHearts(val, pos) -- icon is int
		for i = 1, val do
			local s = Sprite()
			s:Load("gfx/ui/ui_glassheart.anm2", true)
			s:Play("Idle", true)
			s:RenderLayer(0, pos + (Vector(12, 0)*i))
		end
	end
	
	function EU.round(num, place)
		local n = (num*place - math.floor(num)*place)/place
		if (n*(place) - math.floor(n*(place))) < 0.5 then
			return math.floor(num*place)/place
		else
			return math.ceil(num*place)/place
		end
	end
	
	function EU.percent(Num1, Num2, Round, Invert)
		local Percentage = (Num1 / Num2) * 100
		Percentage = (math.floor(Percentage*100)/100)
		if Round then
			Percentage = math.floor(Percentage)
		end
		if Invert then
			Percentage = math.abs(Percentage-100)
		end
		return Percentage
	end

	function EU.unitvector(v)
		return v / math.sqrt(v.X * v.X + v.Y * v.Y)
	end

	function EU.random(x, y, rng)
		x = EU.round(x, 1)
		y = EU.round(y, 1)
		if not y then
			y = x
			x = 1
		end
		return (rng:RandomInt(y - x + 1)) + x
	end

	function EU.randomfloat(x, y, rng)
		x = EU.round(x, 1)
		y = EU.round(y, 1)
		if not y then
			y = x
			x = 0
		end
		x = x * 1000
		y = y * 1000
		return math.floor((rng:RandomInt(y - x + 1)) + x) / 1000
	end

	function EU.getrandomunitvec()
		return EU.unitvector(Vector(EU.randomfloat(-1, 1, EU.rng), EU.randomfloat(-1, 1, EU.rng)))
	end
	
	function EU.GetBitList(Array, bitint, eff) -- adds both true and false bits
		local BitList = {}
		for i, e in pairs(Array) do
			if eff then
				if (bitint & e > 0) then
					table.insert(BitList, i - 1)
				end
			else
				if (bitint & e > 0) then
					BitList[i - 1] = true
				else
					BitList[i - 1] = false
				end
			end
		end
		return BitList
	end

	function EU.GetSoulHearts(p) -- only for soul soul hearts, not black
		local blackheartidx = EU.GetBitList(EU.blackheartarray, p:GetBlackHearts(), true)
		local bhearts = #blackheartidx * 2
		local soulhearts = p:GetSoulHearts() - bhearts
		if soulhearts % 2 == 1 then
			return soulhearts - 1
		else
			return soulhearts 
		end
	end

	function EU.GetBlackHearts(p) -- returns amount of black hearts
		local blackheartidx = EU.GetBitList(EU.blackheartarray, p:GetBlackHearts(), true)
		local bhearts = #blackheartidx * 2
		local soulhearts = p:GetSoulHearts() - bhearts
		if soulhearts % 2 == 1 then
			return p:GetSoulHearts() - soulhearts - 1
		else
			return p:GetSoulHearts() - soulhearts
		end
	end
	
	function EU.GetBoneHeartIDX(p)
		local t = {}
		local soulspace = ((math.ceil(p:GetHeartLimit() / 2) - math.ceil(p:GetMaxHearts() / 2)) - p:GetBrokenHearts()) - 1
		for i = 0, soulspace do
			if p:IsBoneHeart(i) == true then
				t[i] = true
			else
				t[i] = false
			end
		end
		return t
	end
	
	function EU.GetBlackHeartIDX(player) -- adapted from cucco's code
		local soulHearts = player:GetSoulHearts()
		local t = {}
		local currentSoulHeart = 0
		for i = 0, (math.ceil(player:GetSoulHearts() / 2) + player:GetBoneHearts()) - 1 do
			if not player:IsBoneHeart(i) then
				if player:IsBlackHeart(currentSoulHeart + 1) then
					t[i] = true
				else
					t[i] = false
				end
				currentSoulHeart = currentSoulHeart + 2
			end
		end
		return t
	end
	
	function EU.GetSoulHeartIDX(p)
		local blackIDX = EU.GetBlackHeartIDX(p)
		local boneIDX = EU.GetBoneHeartIDX(p)
		local t = {}
		local soulspace = ((math.ceil(p:GetHeartLimit() / 2) - math.ceil(p:GetMaxHearts() / 2)) - p:GetBrokenHearts()) - 1
		
		local soulcount = math.ceil(EU.GetSoulHearts(p) / 2)
		local blackcount = math.ceil(EU.GetBlackHearts(p) / 2)
		local bonecount = p:GetBoneHearts()
		
		local cumulSoul = bonecount + blackcount + soulcount - 1
		
		for i = 0, cumulSoul do
			if boneIDX[i] == false and blackIDX[i] == false then
				t[i] = true
			else
				t[i] = false
			end
		end
		
		return t
	end
	
	function EU.GetBrokenHeartIDX(p)
		local t = {}
		local bHearts = p:GetBrokenHearts()
		local takenidx = math.ceil((p:GetSoulHearts() + p:GetMaxHearts()) / 2) + p:GetBoneHearts()
		local pHeartLimit = math.ceil(p:GetHeartLimit() / 2) - 1 + p:GetBrokenHearts()
		
		for i = 0, pHeartLimit do
			if i > takenidx and bHearts > 0 then
				t[i - 1] = true
				bHearts = bHearts - 1
			else
				t[i - 1] = false
			end
		end
		return t
	end
	
	function EU.GetRedHeartIDX(p)
		local t = {}
		local redspace = math.ceil(p:GetMaxHearts() / 2) - 1
		for i = 0, redspace do
			t[i] = true
		end
		return t
	end
	
	local function hidx(p)
		local blackIDX = EU.GetBlackHeartIDX(p)
		local boneIDX = EU.GetBoneHeartIDX(p)
		local soulIDX = EU.GetSoulHeartIDX(p)
		local brokenIDX = EU.GetBrokenHeartIDX(p)
		local redIDX = EU.GetRedHeartIDX(p)
		local soulstart = math.ceil(p:GetMaxHearts() / 2)
		local soulspace = ((math.ceil(p:GetHeartLimit() / 2) - soulstart) - p:GetBrokenHearts()) - 1
		local pHeartLimit = math.ceil(p:GetHeartLimit() / 2) - 1 + p:GetBrokenHearts()
		
		
		local t = {}
		
		for i = 0, pHeartLimit do
			t[i] = {TYPE, VARIANT, SUBTYPE, VALUE}
		end
		
		for i = 0, soulspace do
			if blackIDX[i] == true then
				t[i + soulstart] = {TYPE = "soul", VARIANT = "black"}
			elseif boneIDX[i] == true then
				t[i + soulstart].TYPE = "bone"
			elseif soulIDX[i] == true then
				t[i + soulstart].TYPE = "soul"
			end
		end
		
		for i = 0, pHeartLimit do
			if brokenIDX[i] == true then
				t[i].TYPE = "broken"
			elseif redIDX[i] == true then
				t[i].TYPE = "red"
			end
		end
		
		return t
	end
	
	function EU.GetRottenHeartIDX(p)
		local t = {}
		local hidx = hidx(p)
		local pHeartLimit = math.ceil(p:GetHeartLimit() / 2) - 1 + p:GetBrokenHearts()
		local emptyhearts =  math.ceil(((p:GetMaxHearts() / 2) + p:GetBoneHearts()) - (math.ceil(p:GetHearts() / 2)))
		local rHearts = p:GetRottenHearts()
		local currhearts = (math.ceil(p:GetHearts() / 2)) - rHearts
		
		for i = 0, pHeartLimit do
			if i >= currhearts then
				if rHearts > 0 and (hidx[i].TYPE == "red" or hidx[i].TYPE == "bone") then
					t[i] = true
					rHearts = rHearts - 1
				else
					t[i] = false
				end
			end
		end
		
		return t
	end
	
	function EU.GetHeartIndex(p)
		local t = hidx(p)
		local ridx = EU.GetRottenHeartIDX(p)
		
		local pHeartLimit = math.ceil(p:GetHeartLimit() / 2) - 1 + p:GetBrokenHearts()
		
		for i = 0, pHeartLimit do
			if ridx[i] == true then
				t[i].VARIANT = "rot"
			end
		end
		
		return t
	end
	
	function EU.GetHeartByIndex(idx)
		return EU.GetHeartIndex(p)[idx]
	end
	
	function printhearts(p)
		local hidx = EU.GetHeartIndex(p)
		local pHeartLimit = math.ceil(p:GetHeartLimit() / 2) - 1 + p:GetBrokenHearts()
		for i = 0, pHeartLimit do
			print(hidx[i].TYPE, hidx[i].VARIANT, hidx[i].SUBTYPE)
		end
	end
	
	function EU.doWavedashPoof(p, poofcol)
		for i = 1, EU.random(1, 3, EU.rng) do
			local poof = Isaac.Spawn(1000, EU.EffectVariant.DASHPOOF, 0, p.Position + p.Velocity*3, p.Velocity + EU.getrandomunitvec(), p):ToEffect()
			poof.Color = poofcol
			local fd = poof:GetData()
			local fs = poof:GetSprite()
			if fd.EU == nil then fd.EU = {} end
			if not fd.EU.init then
				fs:SetFrame(EU.random(1, 10, EU.rng))
				fd.EU.init = true
			end
		end
	end
	
	
	
	--[[function EU.GetHeartIndex(p) -- returns heart index
		local baseidx = EU.GetBitList(EU.blackheartarray, p:GetBlackHearts(), false)
		local bhearts = #EU.GetBitList(EU.blackheartarray, p:GetBlackHearts(), true)
		
		local nummax = math.ceil(p:GetMaxHearts()/2)
		local numbroke = math.abs(p:GetBrokenHearts())
		
		local idx = {}
		
		for i = 1, #baseidx do
			idx[i + nummax] = baseidx[i]
		end
		for i = 1, nummax do
			idx[i] = "red"
		end
		for i = 0, numbroke do
			idx[math.abs(i - 13)] = "broken"
		end
		
		for i = 1, #idx do
			if i > 12 then
				idx[i] = nil
			end
			if idx[i] == true then
				idx[i] = "black"
			end
			if idx[i] == false then
				if p:IsBoneHeart(i) == true then
					idx[i + nummax] = "bone"
				end
			end
		end
		
		return idx
	end]]

	EU.blackheartarray = {
		[1] = 1<<0,
		[2] = 1<<1,
		[3] = 1<<2,
		[4] = 1<<3,
		[5] = 1<<4,
		[6] = 1<<5,
		--2nd row
		[7] = 1<<6,
		[8] = 1<<7,
		[9] = 1<<8,
		[10] = 1<<9,
		[11] = 1<<10,
		[12] = 1<<11,
	}
	
	EU.accuracyStat = { -- cap is 360.
		[PlayerType.PLAYER_POSSESSOR] = 360,
		[PlayerType.PLAYER_ISAAC] = 357,
		[PlayerType.PLAYER_MAGDALENA] = 357,
		[PlayerType.PLAYER_CAIN] = 350,
		[PlayerType.PLAYER_JUDAS] = 357,
		[PlayerType.PLAYER_XXX] = 357,
		[PlayerType.PLAYER_EVE] = 357,
		[PlayerType.PLAYER_SAMSON] = 357,
		[PlayerType.PLAYER_AZAZEL] = 360, -- special weapons all have 360.
		[PlayerType.PLAYER_LAZARUS] = 357,
		[PlayerType.PLAYER_EDEN] = -1, -- -1 because its eden. i will set up a randomization function for -1 characters.
		[PlayerType.PLAYER_THELOST] = 357,
		[PlayerType.PLAYER_LAZARUS2] = 355,
		[PlayerType.PLAYER_BLACKJUDAS] = 357,
		[PlayerType.PLAYER_LILITH] = 357,
		[PlayerType.PLAYER_KEEPER] = 360, -- so his triple shot doesnt look stupid.
		[PlayerType.PLAYER_APOLLYON] = 357,
		[PlayerType.PLAYER_THEFORGOTTEN] = 357, -- this is technically only the soul's stats.
		[PlayerType.PLAYER_THESOUL] = 357, -- ^^^ must be the same
		[PlayerType.PLAYER_BETHANY] = 357,
		[PlayerType.PLAYER_JACOB] = 350,
		[PlayerType.PLAYER_ESAU] = 360,
		
		-- TAINTED --
		
	}
	
	EU.doorsuffix = { -- EU.doorsuffix[room:GetBackdropType()]
		[1] = {png = "basement", anm2 = "normal"},
		[2] = {png = "cellar", anm2 = "normal"},
		[3] = {png = "burningbasement", anm2 = "normal"},
		[4] = {png = "caves", anm2 = "normal"},
		[5] = {png = "catacombs", anm2 = "normal"},
		[6] = {png = "floodedcaves", anm2 = "normal"},
		[7] = {png = "depths", anm2 = "normal"},
		[8] = {png = "necropolis", anm2 = "normal"},
		[9] = {png = "dankdepths", anm2 = "normal"},
		[10] = {png = "womb", anm2 = "womb"},
		[11] = {png = "utero", anm2 = "womb"},
		[12] = {png = "scarredwomb", anm2 = "womb"},
		[13] = {png = "bluewomb", anm2 = "corpse"},
		[14] = {png = "sheol", anm2 = "normal"},
		[15] = {png = "cathedral", anm2 = "normal"},
		[16] = {png = "darkroom", anm2 = "darkroom"},
		[17] = {png = "chest", anm2 = "normal"},
		
		[19] = {png = "library", anm2 = "normal"},
		[20] = {png = "shop", anm2 = "normal"},
		--[21] = {png = "isaacsroom", anm2 = "normal"},
		--[22] = {png = "isaacsroom", anm2 = "normal"},
		
		--[24] = {png = "dice", anm2 = "normal"},
		--[25] = {png = "arcade", anm2 = "normal"},
		
		--[30] = {png = "sheol", anm2 = "normal"},
	
		[31] = {png = "downpour", anm2 = "normal"},
		[32] = {png = "mines", anm2 = "normal"},
		[33] = {png = "mausoleum", anm2 = "normal"},
		[34] = {png = "corpse", anm2 = "corpse"},
		
		[40] = {png = "mausoleum", anm2 = "normal"},
		[41] = {png = "mausoleum", anm2 = "normal"},
		
		[43] = {png = "corpse2", anm2 = "corpse"},
		[44] = {png = "corpse3", anm2 = "corpse"},
		--[45] = {png = "dross", anm2 = "normal"},
		--[46] = {png = "ashpit", anm2 = "normal"},
		--[47] = {png = "gehenna", anm2 = "normal"},
		--[48] = {png = "gehenna", anm2 = "normal"},
		
		--[58] = {png = "mines", anm2 = "normal"},
		--[59] = {png = "ashpit", anm2 = "normal"},
	}
	
	EU.hudthemes = {
		[0] = { -- default
			[0] = {r = 1, g = 1, b = 1},
			[1] = {r = 1, g = 1, b = 1},
			[2] = {r = 1, g = 1, b = 1},
			[3] = {r = 0.5, g = 0.5, b = 0.75},
			[4] = {r = 0.5, g = 0.5, b = 0.75},
			[5] = {r = 0.5, g = 0.5, b = 0.75},
			[6] = {r = 0.5, g = 0.5, b = 0.75},
			ang = {r = 1, g = 1, b = 1},
			dev = {r = 1, g = 0.2, b = 0.2},
			name = "default",
			overhead = {r = 1, g = 1, b = 1},
			Type = 0
		},
		[-1] = { -- default
			[0] = {r = 1, g = 1, b = 1},
			[1] = {r = 1, g = 1, b = 1},
			[2] = {r = 1, g = 1, b = 1},
			[3] = {r = 0.75, g = 0.5, b = 0.5},
			[4] = {r = 0.75, g = 0.5, b = 0.5},
			[5] = {r = 0.75, g = 0.5, b = 0.5},
			[6] = {r = 0.75, g = 0.5, b = 0.5},
			ang = {r = 1, g = 1, b = 1},
			dev = {r = 1, g = 0.2, b = 0.2},
			name = "esau",
			overhead = {r = 1, g = 1, b = 1},
			Type = 0
		},
		[1] = { -- light
			[0] = {r = 1, g = 1, b = 1},
			[1] = {r = 1, g = 1, b = 1},
			[2] = {r = 1, g = 1, b = 1},
			[3] = {r = 1, g = 1, b = 1},
			[4] = {r = 1, g = 1, b = 1},
			[5] = {r = 1, g = 1, b = 1},
			[6] = {r = 1, g = 1, b = 1},
			ang = {r = 1, g = 1, b = 1},
			dev = {r = 1, g = 1, b = 1},
			name = "light",
			overhead = {r = 1, g = 1, b = 1},
			Type = 0
		},
		[2] = { -- dark
			[0] = {r = 0.2, g = 0.3, b = 0.5},
			[1] = {r = 0.2, g = 0.3, b = 0.5},
			[2] = {r = 0.2, g = 0.3, b = 0.5},
			[3] = {r = 0.2, g = 0.3, b = 0.5},
			[4] = {r = 0.2, g = 0.3, b = 0.5},
			[5] = {r = 0.2, g = 0.3, b = 0.5},
			[6] = {r = 0.2, g = 0.3, b = 0.5},
			ang = {r = 1, g = 1, b = 1},
			dev = {r = 1, g = 0.2, b = 0.2},
			name = "dark",
			overhead = {r = 0.5, g = 0.5, b = 0.75},
			Type = 0
		},
		[3] = { -- rbw
			[0] = {r = 1, g = 0, b = 0},
			[1] = {r = 1, g = 0, b = 0},
			[2] = {r = 1, g = 0, b = 0},
			[3] = {r = 0.25, g = 0.25, b = 0.25},
			[4] = {r = 0.25, g = 0.25, b = 0.25},
			[5] = {r = 0.25, g = 0.25, b = 0.25},
			[6] = {r = 0.25, g = 0.25, b = 0.25},
			ang = {r = 1, g = 1, b = 1},
			dev = {r = 1, g = 0, b = 0},
			name = "rbw",
			overhead = {r = 1, g = 1, b = 1},
			Type = 0
		},
		[4] = { -- pride
			[0] = {r = 1, g = 0.1, b = 0.1},
			[1] = {r = 1, g = 0.5, b = 0.1},
			[2] = {r = 1, g = 1, b = 0.1},
			[3] = {r = 0.1, g = 1, b = 0.1},
			[4] = {r = 0.1, g = 1, b = 1},
			[5] = {r = 0.1, g = 0.1, b = 1},
			[6] = {r = 1, g = 0.1, b = 1},
			dev = {r = 0.8, g = 0.75, b = 0.05},
			ang = {r = 0.35, g = 0.35, b = 0.35},
			name = "pride",
			overhead = {r = 1, g = 1, b = 1},
			Type = 0
		},
		[5] = { --smart
			[0] = {r = 0.1, g = 1, b = 0.5},
			[1] = {r = 0.1, g = 1, b = 1},
			[2] = {r = 1, g = 0.2, b = 0.2},
			[3] = {r = 0.5, g = 0.5, b = 0.5},
			[4] = {r = 0.5, g = 0.5, b = 0.5},
			[5] = {r = 0.5, g = 0.5, b = 0.5},
			[6] = {r = 0.5, g = 0.5, b = 0.5},
			ang = {r = 1, g = 1, b = 1},
			dev = {r = 1, g = 0.2, b = 0.2},
			name = "smart",
			overhead = {r = 1, g = 1, b = 1},
			Type = 0
		},
		[6] = { --delirious
			[0] = {r = 1, g = 1, b = 0.2},
			[1] = {r = 1, g = 1, b = 0.2},
			[2] = {r = 1, g = 1, b = 0.2},
			[3] = {r = 1, g = 1, b = 1},
			[4] = {r = 1, g = 1, b = 1},
			[5] = {r = 1, g = 1, b = 1},
			[6] = {r = 1, g = 1, b = 1},
			ang = {r = 1, g = 1, b = 1},
			dev = {r = 1, g = 1, b = 0.2},
			name = "delirious",
			overhead = {r = 1, g = 1, b = 0.2},
			Type = 0
		},
		[7] = { --2SPOOKY
			[0] = {r = 0.75, g = 0, b = 0},
			[1] = {r = 0.75, g = 0, b = 0},
			[2] = {r = 0.75, g = 0, b = 0},
			[3] = {r = 0.75, g = 0, b = 0},
			[4] = {r = 0.75, g = 0, b = 0},
			[5] = {r = 0.75, g = 0, b = 0},
			[6] = {r = 0.75, g = 0, b = 0},
			ang = {r = 0.75, g = 0, b = 0},
			dev = {r = 0.75, g = 0, b = 0},
			name = "2SPOOKY",
			overhead = {r = 0.75, g = 0, b = 0},
			Type = 0
		},
	}
	
	EU.fonts = {
		pfseventempesta = "../mods/".. modfolder .."/".. "resources/font/".."pfseventempesta.fnt",
		lambda = {
			normal = "../mods/".. modfolder .."/".. "resources/font/".."lambda.fnt",
			mini = "../mods/".. modfolder .."/".. "resources/font/".."lambdamini.fnt",
			scribble = "../mods/".. modfolder .."/".. "resources/font/".."lambdascribble.fnt",
		}
	}
	
	EU.menu = {
		spr = Sprite(),
		opt = {}
	}
	
	--[[
	EU.heartIDXdata = { -- cap is 360.
		[-2] = {Type = "Broken", Name = ""},
		[-1] = {Type = "Empty", Name = ""},
		[0] = {Type = "Red", Name = "Empty"},
		[1] = {Type = "Red", Name = "Half"},
		[2] = {Type = "Red", Name = "Full"},
		[3] = {Type = "Soul", Name = "Half"},
		[4] = {Type = "Soul", Name = "Full"},
		[5] = {Type = "Black", Name = "Half"},
		[6] = {Type = "Black", Name = "Full"},
		[7] = {Type = "Bone", Name = "Empty"},
		[8] = {Type = "Bone", Name = "Half"},
		[9] = {Type = "Bone", Name = "Full"},
		[10] = {Type = "Rotten", Name = "Half"},
		[11] = {Type = "Rotten", Name = "Full"},
	}]]
	
	EU.CollectibleType = {
		COLLECTIBLE_BLUEBIRD = Isaac.GetItemIdByName("Blue Bird"),
		COLLECTIBLE_BROKENMIRROR = Isaac.GetItemIdByName("Broken Mirror"),
		COLLECTIBLE_FRAGILEHEART = Isaac.GetItemIdByName("Fragile Heart"),
		COLLECTIBLE_EPIPHORA = Isaac.GetItemIdByName(" Epiphora"),
		COLLECTIBLE_CRICKETSPAW = Isaac.GetItemIdByName("Cricket's Paw"),
		COLLECTIBLE_CRICKETSTAIL = Isaac.GetItemIdByName("Cricket's Tail"),
		COLLECTIBLE_OCCAMSRAZOR = Isaac.GetItemIdByName("Occam's Razor"),
		COLLECTIBLE_BOOKOFLOVE = Isaac.GetItemIdByName("Book of Love"),
		COLLECTIBLE_CRUCIBLE = Isaac.GetItemIdByName("The Crucible"),
		COLLECTIBLE_STRAWBERRYMILK = Isaac.GetItemIdByName("Strawberry Milk"),
	}
	
	EU.PlayerType = {
		PLAYER_ABEL = Isaac.GetPlayerTypeByName("Abel", false),
		PLAYER_ABEL_B = Isaac.GetPlayerTypeByName("Abel", true),
		
		PLAYER_ESAU_E = Isaac.GetPlayerTypeByName("shitsau", false),
		PLAYER_JACOB_E = Isaac.GetPlayerTypeByName("jabob", false),
	}
	
	EU.NullItemID = {
		ID_ABEL = Isaac.GetCostumeIdByPath("gfx/characters/character_abelhair.anm2")
	}
	
	EU.PickupVariant = {
		PICKUP_BIGGRABBAG = Isaac.GetEntityVariantByName("Big Grab Bag")
	}
	
	EU.EffectVariant = {
		CHAT = Isaac.GetEntityVariantByName("Chat Text"),
		DASHPOOF = Isaac.GetEntityVariantByName("DashPoof")
	}
	
	if EU.sd.s == nil then EU.sd.s = {} end -- hud offset
	if EU.sd.s[1] == nil then EU.sd.s[1] = 0 end -- hud offset
	if EU.sd.glasshearts == nil then EU.sd.glasshearts = {} end
	if EU.sd.hascollectible == nil then EU.sd.hascollectible = {} end
	if EU.sd.cruciblecharge == nil then EU.sd.cruciblecharge = {} end
	if EU.sd.cricketpawboost == nil then EU.sd.cricketpawboost = {} end
	if EU.sd.s[2] == nil then EU.sd.s[2] = 0 end -- hud extra
	if EU.sd.s[3] == nil then EU.sd.s[3] = false end -- hud show on cache
	if EU.sd.ach == nil then 
		EU.sd.ach = {
			[1] = {name = "Occam's Razor", gfx = achgfxpath .. "001_occamsrazor.png", unlock = false, item = EU.CollectibleType.COLLECTIBLE_OCCAMSRAZOR},
			[2] = {name = "Book of Love", gfx = achgfxpath .. "002_bookoflove.png", unlock = false, item = EU.CollectibleType.COLLECTIBLE_BOOKOFLOVE},
		} 
	end -- hud offset

	
end

do -- engine callbacks --	
	EU.menu.spr:Load("gfx/ui/eternalmenu.anm2", true)
	
	local EUfont = {
		[0] = Font(),
		[1] = Font(),
		[2] = Font(),
		[3] = Font(),
		[4] = Font(),
		[5] = Font()
	}
	
	EUfont[0]:Load(EU.fonts.lambda.mini)
	EUfont[1]:Load(EU.fonts.pfseventempesta)
	EUfont[2]:Load("font/upheavalmini.fnt")
	EUfont[3]:Load(EU.fonts.lambda.normal)
	EUfont[4]:Load(EU.fonts.lambda.scribble)
	EUfont[5]:Load("font/luaminioutlined.fnt")
	
	EU.menu.opt[1] = {name = "HUD offset", [0] = EU.sd.s[1], maxi = 10}
	EU.menu.opt[2] = {name = "CHUD", [0] = EU.sd.s[2], maxi = 2}
	EU.menu.opt[3] = {name = "CHUD Cache", [0] = EU.sd.s[3], maxi = nil} -- this means its a string or a bool
	--EU.sd.s[2]
	
	EU:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, function(_, p)
		local d = p:GetData()
		if d.EU == nil then d.EU = {} end
		if d.EU.updates == nil then d.EU.updates = 0 end
		if EU.game:GetFrameCount() == 0 then
			local seed = EU.game:GetSeeds():GetStartSeed()
			if EU:HasData() then
				EU.sd = json.decode(Isaac.LoadModData(EU))
			end
			EU.itempool = EU.game:GetItemPool()
			
			for i = 1, EU.len(EU.sd.ach) do
				if EU.sd.ach[i] and EU.sd.ach[i].unlock == false then
					if EU.sd.ach[i].item then
						EU.itempool:RemoveCollectible(EU.sd.ach[i].item)
						print("removed", EU.sd.ach[i].name, "from unlocks")
					end
				end
			end
			EU.rng:SetSeed(seed, 35)
			EU.sd.lazIIboost = {}
			EU.sd.occamboost = {}
			EU.sd.cricketpawboost = {}
			EU.sd.glasshearts = {}
			EU.sd.hascollectible = {}
		else
			if EU:HasData() then
				EU.sd = json.decode(Isaac.LoadModData(EU))
			end
		end
		if p:GetPlayerType() == PlayerType.PLAYER_LAZARUS or p:GetPlayerType() == PlayerType.PLAYER_LAZARUS_B or p:GetPlayerType() == PlayerType.PLAYER_LAZARUS2 or p:GetPlayerType() == PlayerType.PLAYER_LAZARUS2_B then
			if p:GetPlayerType() ~= PlayerType.PLAYER_LAZARUS2 and p:GetPlayerType() ~= PlayerType.PLAYER_LAZARUS2_B then
				p:AddSoulHearts(2)
				p:AddMaxHearts(-2)
			end
			if p:GetPlayerType() == PlayerType.PLAYER_LAZARUS_B then
				p:SetPill(1, EU.random(1, 13, EU.rng))
			end
		elseif p:GetPlayerType() == EU.PlayerType.PLAYER_ABEL then
			p:AddCollectible(EU.CollectibleType.COLLECTIBLE_BOOKOFLOVE, 3, true, 0, 0)
			p:AddNullCostume(EU.NullItemID.ID_ABEL)
			--p:UsePill(PillEffect.PILLEFFECT_RANGE_UP, PillColor.PILL_NULL, UseFlag.USE_MIMIC | UseFlag.USE_NOANIM | UseFlag.USE_NOANNOUNCER)
			p:AddBombs(2)
		elseif p:GetPlayerType() == EU.PlayerType.PLAYER_ESAU_E then
			d.EU.fp = EU.addplayer(p, EU.PlayerType.PLAYER_JACOB_E, p.ControllerIndex, true, true)
			if not d.EU.chain or not d.EU.chain:Exists() then
				d.EU.chain = EU.SpawnChain(p, d.EU.fp, 10)
			end
		elseif p:GetPlayerType() == EU.PlayerType.PLAYER_JACOB_E then
			p:GetSprite():Load("gfx/001.000_player_jacobC.anm2", true)
			p:GetSprite():Update()
			p:PlayExtraAnimation("floor")
		end
	end)

	
	
	EU:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, function(_, p)
		--print(EU.NullItemID.ID_ABEL)
		--print(p:GetEffects():GetNullEffect(7).Count)
		local playerseed = p:GetCollectibleRNG(0):GetSeed()
		local d = p:GetData()
		local s = p:GetSprite()
		if p:GetBrokenHearts() >= 11 then
			if EU.sd.ach[1].unlock == false then
				EU.sd.ach[1].unlock = true
				EtAPI.ShowCustomAchievement(EU.sd.ach[1].gfx)
			end
		end
		if d.EU == nil then d.EU = {} end
		d.EU.updates = d.EU.updates + 1
		if d.EU.poofcolor == nil then d.EU.poofcolor = {r = 1, g = 1, b = 1, a = 0.25} end
		local poofcol = Color(d.EU.poofcolor.r, d.EU.poofcolor.g, d.EU.poofcolor.b, d.EU.poofcolor.a, 0, 0, 0)
		local dashpoofcol = Color(d.EU.poofcolor.r, d.EU.poofcolor.g, d.EU.poofcolor.b, d.EU.poofcolor.a/2, 0, 0, 0)
		if d.EU.GlassHearts == nil then d.EU.GlassHearts = 0 end
		if d.EU.GlassHearts > 5 then d.EU.GlassHearts = 6 end
		if d.EU.GlassHearts < 1 then d.EU.GlassHearts = 0 end
		
		if p:GetPlayerType() == EU.PlayerType.PLAYER_JACOB_E then
			if s:GetFilename() ~= "gfx/001.000_player_jacobC.anm2" and not p:IsCoopGhost() then
				s:Load("gfx/001.000_player_jacobC.anm2", true)
				s:Update()
			end
		else
			if s:GetFilename() == "gfx/001.000_player_jacobC.anm2" or p:IsCoopGhost() then
				s:Load("gfx/001.000.player.anm2", true)
				s:Update()
			end
		end
		
		if d.EU.dubboost == nil then d.EU.dubboost = 0 end
		if d.EU.dubboost < 0 then 
			d.EU.dubboost = 0 
			p:AddCacheFlags(CacheFlag.CACHE_SPEED)
			p:EvaluateItems()
		end
		if d.EU.dubboost > 0.5 then 
			d.EU.dubboost = 0.5 
			p:AddCacheFlags(CacheFlag.CACHE_SPEED)
			p:EvaluateItems()
		end
		if p:GetMovementDirection() ~= -1 and not p.CanFly then
			for i = 1, EU.random(0, 1, EU.rng) do
				if EU.random(1, 8, EU.rng) == 1 then
					local poof = Isaac.Spawn(1000, EU.EffectVariant.DASHPOOF, 0, p.Position - p.Velocity*2, -(p.Velocity/2) + EU.getrandomunitvec(), p):ToEffect()
					poof.Color = dashpoofcol
					local fd = poof:GetData()
					local fs = poof:GetSprite()
					if fd.EU == nil then fd.EU = {} end
					if not d.EU.init then
						fs:SetFrame(EU.random(7, 10, EU.rng))
						fd.EU.init = true
					end
				end
			end
		end
		if d.EU.dubboost > 0 then
			--d.EU.dubboost = d.EU.dubboost - 0.00075 * ((p.MoveSpeed^2) * 4)
			--p:AddCacheFlags(CacheFlag.CACHE_SPEED)
			--p:EvaluateItems()
		end
		
		if p:HasCollectible(CollectibleType.COLLECTIBLE_EPIPHORA) then
			p:RemoveCollectible(CollectibleType.COLLECTIBLE_EPIPHORA)
			p:AddCollectible(EU.CollectibleType.COLLECTIBLE_EPIPHORA, 0, false, 0, 0)
		end
		if p:HasCollectible(EU.CollectibleType.COLLECTIBLE_EPIPHORA) then
			local tps = 30 / (p.MaxFireDelay + 1)
			if d.EU.epiphboost == nil then d.EU.epiphboost = 0 end
			if d.EU.epiphboost < 0 then 
				d.EU.epiphboost = 0 
				p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
				p:EvaluateItems()
			end
			if p:GetFireDirection() ~= Direction.NO_DIRECTION then
				if d.EU.epiphboost == nil then d.EU.epiphboost = 0 end
				if d.EU.epiphboost < 0.4 then 
					d.EU.epiphboost = d.EU.epiphboost + 0.001 / (tps^0.5)
					p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
					p:EvaluateItems()
				end
			else
				if d.EU.epiphboost == nil then d.EU.epiphboost = 0 end
				if d.EU.epiphboost > 0 then 
					d.EU.epiphboost = d.EU.epiphboost - 0.0005 * (tps)
					p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
					p:EvaluateItems()
				end
			end
		end
		
		if p:HasCollectible(EU.CollectibleType.COLLECTIBLE_BROKENMIRROR) then
			if not EU.sd.hascollectible[tostring(playerseed)] then
				EU.sd.hascollectible[tostring(playerseed)] = {}
				if EU.sd.hascollectible[tostring(playerseed)].item == nil then
					EU.sd.hascollectible[tostring(playerseed)].item = {}
					EU.sd.hascollectible[tostring(playerseed)].item[tostring(EU.CollectibleType.COLLECTIBLE_BROKENMIRROR)] = true
					d.EU.GlassHearts = d.EU.GlassHearts + 3
				end
			end
		else
			if EU.sd.hascollectible and EU.sd.hascollectible[tostring(playerseed)] then
				if EU.sd.hascollectible[tostring(playerseed)].item and EU.sd.hascollectible[tostring(playerseed)].item[tostring(EU.CollectibleType.COLLECTIBLE_BROKENMIRROR)] then
					if EU.sd.hascollectible[tostring(playerseed)].item[tostring(EU.CollectibleType.COLLECTIBLE_BROKENMIRROR)] then
						EU.sd.hascollectible[tostring(playerseed)].item[tostring(EU.CollectibleType.COLLECTIBLE_BROKENMIRROR)] = nil
					end
				end
			end
		end
		
		if not d.EU.Initialized then
			if not p.Parent then
				if not d.EU.ControllerName then
					d.EU.ControllerName = EU:getController(p)
				end
			end
			if EU.sd.glasshearts and EU.sd.glasshearts[tostring(playerseed)] then
				if d.EU == nil then d.EU = {} end
				if d.EU.GlassHearts == nil then d.EU.GlassHearts = 0 end
				d.EU.GlassHearts = EU.sd.glasshearts[tostring(playerseed)]
			end
			if d.EU.IsKillable == nil then d.EU.IsKillable = true end
			d.EU.Initialized = true
		end
		
		if d.EU.IsKillable and d.EU.IsKillable == false then
			p:AddEntityFlags(EntityFlag.FLAG_NO_BLOOD_SPLASH)
		else
			if p:HasEntityFlags(EntityFlag.FLAG_NO_BLOOD_SPLASH) then
				p:ClearEntityFlags(EntityFlag.FLAG_NO_BLOOD_SPLASH)
			end
		end
		
		if EU.sd.cricketpawboost[tostring(playerseed)] == nil then EU.sd.cricketpawboost[tostring(playerseed)] = 0 end
		if EU.sd.glasshearts[tostring(playerseed)] == nil then EU.sd.glasshearts[tostring(playerseed)] = 0 end
		EU.sd.glasshearts[tostring(playerseed)] = d.EU.GlassHearts
		
		if p:GetEffects():HasCollectibleEffect(CollectibleType.COLLECTIBLE_HOLY_MANTLE) then
			d.EU.HasHolyMantle = true
		else
			if d.EU.HasHolyMantle then
				p:TakeDamage(1, DamageFlag.DAMAGE_NOKILL | DamageFlag.DAMAGE_NO_PENALTIES | DamageFlag.DAMAGE_FAKE, EntityRef(p), 0)
				p:SetMinDamageCooldown(80)
				d.EU.HasHolyMantle = false
			end
		end
		if d.EU.Accuracy == nil then
			if EU.accuracyStat[p:GetPlayerType()] and EU.accuracyStat[p:GetPlayerType()] ~= -1 then
				d.EU.Accuracy = EU.accuracyStat[p:GetPlayerType()]
			elseif EU.accuracyStat[p:GetPlayerType()] and EU.accuracyStat[p:GetPlayerType()] == -1 then
				d.EU.Accuracy = EU.random(350, 360, EU.rng)
			elseif not EU.accuracyStat[p:GetPlayerType()] then
				d.EU.Accuracy = 357
			end
		else
			--print(d.EU.Accuracy)
			if d.EU.Accuracy > 360 then d.EU.Accuracy = 360 end
			if d.EU.Accuracy < 0 then d.EU.Accuracy = 0 end
		end
		if p:AreControlsEnabled() and p:GetPlayerType() ~= PlayerType.PLAYER_THEFORGOTTEN_B then
			local room = EU.game:GetRoom()
			local move = p:GetMovementInput()
			--RoomType.ROOM_DUNGEON
			if move.X < 0 and p.Velocity.X > 0 then 
				EU.doWavedashPoof(p, poofcol)
				p.Velocity = Vector(-p.Velocity.X/10, p.Velocity.Y)
			elseif move.X > 0 and p.Velocity.X < 0 then 
				EU.doWavedashPoof(p, poofcol)
				p.Velocity = Vector(-p.Velocity.X/10, p.Velocity.Y)
			end
			
			if move.Y < 0 and p.Velocity.Y > 0 then
				if (room:GetType() == RoomType.ROOM_DUNGEON) == false then
					EU.doWavedashPoof(p, poofcol)
					p.Velocity = Vector(p.Velocity.X, -p.Velocity.Y/10)
				end
			elseif move.Y > 0 and p.Velocity.Y < 0 then
				EU.doWavedashPoof(p, poofcol)
				p.Velocity = Vector(p.Velocity.X, -p.Velocity.Y/10)
			end
			
			if not p.CanFly then
				if p:GetMovementDirection() ~= -1 then
					p.Velocity = p.Velocity + p:GetMovementInput():Normalized() / 1.75
				end
				p:MultiplyFriction(0.9)
			end
		end
		if d.EU.tearcap then
			if p.MaxFireDelay < d.EU.tearcap then
				p.MaxFireDelay = d.EU.tearcap
			end
		end
		if d.EU.fp and d.EU.fp:Exists() and not d.EU.fp:IsDead() then
			local fp = d.EU.fp
			local dist = (fp.Position - p.Position):Length()
			local allowdist = 96
			if dist > allowdist then
				p.Velocity = p.Velocity - (p.Position - fp.Position):Normalized() * ((dist - allowdist) / 24)
				fp.Velocity = fp.Velocity - (fp.Position - p.Position):Normalized() * ((dist - allowdist) / 24)
			end
			if not d.EU.chain or not d.EU.chain:Exists() then
				d.EU.chain = EU.SpawnChain(p, fp, 10)
			end
			if dist <= 12 then
				p.Velocity = p.Velocity + (p.Position - fp.Position) / dist
				fp.Velocity = fp.Velocity + (fp.Position - p.Position) / dist
			end
		elseif d.EU.fp and d.EU.fp:IsDead() then
			local fp = d.EU.fp
			fp:Revive()
			fp:AddSoulHearts(1)
		end
	end)
	
	
	
	EU:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, p, flag)
		local d = p:GetData()
		local playerseed = p:GetCollectibleRNG(0):GetSeed()
		if d.EU == nil then d.EU = {} end
		if d.EU.passeditem == nil then d.EU.passeditem = {} end
		if d.EU.passedtrinket == nil then d.EU.passedtrinket = {} end
		if d.EU.tears == nil then d.EU.tears = 0 end
		if d.EU.temptears == nil then d.EU.temptears = 0 end
		if d.EU.tearcap == nil then d.EU.tearcap = 5 end
		if d.EU.dubboost == nil then d.EU.dubboost = 0 end
		
		if p:GetPlayerType() == PlayerType.PLAYER_LAZARUS2 or p:GetPlayerType() == PlayerType.PLAYER_LAZARUS2_B then
			if not EU.sd.lazIIboost then EU.sd.lazIIboost = {} end
			if not EU.sd.lazIIboost[tostring(playerseed)] then
				EU.sd.lazIIboost[tostring(playerseed)] = 0
			end
			if flag == CacheFlag.CACHE_SPEED then
				p.MoveSpeed = p.MoveSpeed + (0.075 * EU.sd.lazIIboost[tostring(playerseed)])
			end
			if flag == CacheFlag.CACHE_SHOTSPEED then
				p.ShotSpeed = p.ShotSpeed + (0.035 * EU.sd.lazIIboost[tostring(playerseed)])
			end
			if flag == CacheFlag.CACHE_FIREDELAY then
				p.MaxFireDelay = p.MaxFireDelay / 1 - (0.35 * EU.sd.lazIIboost[tostring(playerseed)])
			end
			if flag == CacheFlag.CACHE_DAMAGE then
				p.Damage = p.Damage + p.Damage * ( 0 + (0.075 * EU.sd.lazIIboost[tostring(playerseed)]))
			end
			if flag == CacheFlag.CACHE_LUCK then
				p.Luck = p.Luck + (0.25 * EU.sd.lazIIboost[tostring(playerseed)])
			end
		elseif p:GetPlayerType() == EU.PlayerType.PLAYER_ABEL then
			if flag == CacheFlag.CACHE_DAMAGE then
				p.Damage = p.Damage - (p.Damage / 4)
			end
			if flag == CacheFlag.CACHE_LUCK then
				p.Luck = p.Luck + 2
			end
		end
		
		if not EU.sd.occamboost then EU.sd.occamboost = {} end
		if not EU.sd.occamboost[tostring(playerseed)] then
			EU.sd.occamboost[tostring(playerseed)] = 0
		end
		if flag == CacheFlag.CACHE_SPEED then
			p.MoveSpeed = p.MoveSpeed + (0.095 * EU.sd.occamboost[tostring(playerseed)])
		end
		if flag == CacheFlag.CACHE_SHOTSPEED then
			p.ShotSpeed = p.ShotSpeed + (0.05 * EU.sd.occamboost[tostring(playerseed)])
		end
		if flag == CacheFlag.CACHE_FIREDELAY then
			p.MaxFireDelay = p.MaxFireDelay / 1 - (0.475 * EU.sd.occamboost[tostring(playerseed)])
		end
		if flag == CacheFlag.CACHE_DAMAGE then
			p.Damage = p.Damage + p.Damage * ( 0 + (0.1 * EU.sd.occamboost[tostring(playerseed)]))
		end
		if flag == CacheFlag.CACHE_LUCK then
			p.Luck = p.Luck + (0.5 * EU.sd.occamboost[tostring(playerseed)])
		end
		
		if p:GetPlayerType() == PlayerType.PLAYER_LAZARUS or p:GetPlayerType() == PlayerType.PLAYER_LAZARUS_B then
			if flag == CacheFlag.CACHE_FIREDELAY then
				p.MaxFireDelay = p.MaxFireDelay - (p.MaxFireDelay / 4.1)
			end
			if flag == CacheFlag.CACHE_DAMAGE then
				p.Damage = p.Damage - (p.Damage / 4.55)
			end
		end
		
		if flag == CacheFlag.CACHE_SPEED then
			p.MoveSpeed = p.MoveSpeed + d.EU.dubboost
		end
		
		if flag == CacheFlag.CACHE_FIREDELAY and EU.sd.cricketpawboost[tostring(playerseed)] then
			p.MaxFireDelay = p.MaxFireDelay - EU.sd.cricketpawboost[tostring(playerseed)]
		end
		
		if p:HasCollectible(EU.CollectibleType.COLLECTIBLE_CRICKETSTAIL) then
			local ctcnmod = (p:GetCollectibleNum(EU.CollectibleType.COLLECTIBLE_CRICKETSTAIL, true) - 1) / 10
			if flag == CacheFlag.CACHE_FIREDELAY then
				if d.Init_MaxFireDelay == nil then d.Init_MaxFireDelay = 10 end
				p.MaxFireDelay = d.Init_MaxFireDelay * (p.MaxFireDelay / d.Init_MaxFireDelay) ^ (1.25 + ctcnmod)
			end
			
			if flag == CacheFlag.CACHE_DAMAGE then
				if d.Init_Damage == nil then d.Init_Damage = 3.5 end
				p.Damage = d.Init_Damage * (p.Damage / d.Init_Damage) ^ (1.25 + ctcnmod)
			end
			
			if flag == CacheFlag.CACHE_SPEED then
				if d.Init_MoveSpeed == nil then d.Init_MoveSpeed = 1 end
				p.MoveSpeed = d.Init_MoveSpeed * (p.MoveSpeed / d.Init_MoveSpeed) ^ (1.25 + ctcnmod)
			end
			
			if flag == CacheFlag.CACHE_SHOTSPEED then
				if d.Init_ShotSpeed == nil then d.Init_ShotSpeed = 1 end
				p.ShotSpeed = d.Init_ShotSpeed * (p.ShotSpeed / d.Init_ShotSpeed) ^ (1.25 + ctcnmod)
			end
		end
		
		if p:HasCollectible(EU.CollectibleType.COLLECTIBLE_STRAWBERRYMILK) then
			local cnum = p:GetCollectibleNum(EU.CollectibleType.COLLECTIBLE_STRAWBERRYMILK, true)
			if flag == CacheFlag.CACHE_FIREDELAY then
				if d.Init_MaxFireDelay == nil then d.Init_MaxFireDelay = 10 end
				p.MaxFireDelay = p.MaxFireDelay * (2.25 * cnum)
			end
			
			if flag == CacheFlag.CACHE_DAMAGE then
				if d.Init_Damage == nil then d.Init_Damage = 3.5 end
				p.Damage = p.Damage * (1.25 * cnum)
			end
			
			if flag == CacheFlag.CACHE_TEARCOLOR then
				p.TearColor = Color(0.8, 0.9, 0.9, 1, 0.5, 0, 0.25)
			end
		end
		
		if p:HasCollectible(EU.CollectibleType.COLLECTIBLE_EPIPHORA) then
			if flag == CacheFlag.CACHE_FIREDELAY then
				p.MaxFireDelay = p.MaxFireDelay - p.MaxFireDelay * d.EU.epiphboost
			end
		end
		
		if p:HasCollectible(EU.CollectibleType.COLLECTIBLE_BLUEBIRD) then
			local c = EU.CollectibleType.COLLECTIBLE_BLUEBIRD
			if flag == CacheFlag.CACHE_FIREDELAY then
				if p.MaxFireDelay - p.MaxFireDelay * (d.EU.tears + d.EU.temptears) >= d.EU.tearcap then
					p.MaxFireDelay = p.MaxFireDelay - p.MaxFireDelay * 0.2 * p:GetCollectibleNum(c)
				end
			end
			if not d.EU.passeditem[c .. p:GetCollectibleNum(c)] then
				if d.EU.Accuracy and d.EU.Accuracy + 5 <= 360 then
					d.EU.Accuracy = d.EU.Accuracy + 5
				elseif d.EU.Accuracy + 5 > 360 then
					d.EU.Accuracy = 360
				end
				d.EU.passeditem[c .. p:GetCollectibleNum(c)] = true
			end
		else
			local c = EU.CollectibleType.COLLECTIBLE_BLUEBIRD
			if d.EU.passeditem[c .. p:GetCollectibleNum(c) + 1] then
				if d.EU.Accuracy then
					d.EU.Accuracy = d.EU.Accuracy - 5
				end
				d.EU.passeditem[c .. p:GetCollectibleNum(c) + 1] = nil
			end
		end
		
		if p:HasCollectible(EU.CollectibleType.COLLECTIBLE_BROKENMIRROR) then
			local c = EU.CollectibleType.COLLECTIBLE_BROKENMIRROR
			if flag == CacheFlag.CACHE_FIREDELAY then
				if p.MaxFireDelay - math.floor(p.MaxFireDelay * (d.EU.tears + d.EU.temptears)) >= d.EU.tearcap then
					p.MaxFireDelay = p.MaxFireDelay -  p.MaxFireDelay * 0.3 * p:GetCollectibleNum(c)
				end
			end
			if flag == CacheFlag.CACHE_LUCK then
				p.Luck = p.Luck - 1
			end
		end
		
		if p:HasTrinket(TrinketType.TRINKET_CANCER) then
			local c = TrinketType.TRINKET_CANCER
			if not d.EU.passedtrinket[c] then
				if d.EU.tearcap and d.EU.tearcap - 1 >= 0 then
					d.EU.tearcap = d.EU.tearcap - 1
				elseif d.EU.tearcap - 1 < 0 then
					d.EU.tearcap = 0
				end
				d.EU.passedtrinket[c] = true
			end
		else
			local c = TrinketType.TRINKET_CANCER
			if d.EU.passedtrinket[c] then
				if d.EU.tearcap then
					d.EU.tearcap = d.EU.tearcap + 1
				end
				d.EU.passedtrinket[c] = nil
			end
		end
		
		if p:HasCollectible(CollectibleType.COLLECTIBLE_SOY_MILK) or p:HasCollectible(CollectibleType.COLLECTIBLE_ALMOND_MILK) then
			local c = CollectibleType.COLLECTIBLE_SOY_MILK or CollectibleType.COLLECTIBLE_ALMOND_MILK
			if not d.EU.passeditem[c .. p:GetCollectibleNum(c)] then
				if d.EU.tearcap and d.EU.tearcap - 10 >= 0 then
					d.EU.tearcap = d.EU.tearcap - 10
				elseif d.EU.tearcap - 10 < 0 then
					d.EU.tearcap = 0
				end
				d.EU.passeditem[c .. p:GetCollectibleNum(c)] = true
			end
		else
			local c = CollectibleType.COLLECTIBLE_SOY_MILK
			if d.EU.passeditem[c .. p:GetCollectibleNum(c)] then
				if d.EU.tearcap then
					d.EU.tearcap = d.EU.tearcap + 10
				end
				d.EU.passeditem[c .. p:GetCollectibleNum(c)] = nil
			end
		end
		if EU.sd.s[3] == true then
			d.EU.renderalpha = 50
		end
	end)
	
	
	
	EU:AddCallback(ModCallbacks.MC_POST_TEAR_UPDATE, function(_, t)
		local d = t:GetData()
		if not d.EU then d.EU = {} end
		
		local p
		if t.Parent and t.Parent.Type == 1 then 
			p = t.Parent:ToPlayer() 
		elseif t.Parent and t.Parent.Type == 3 then
			p = t.Parent:ToFamiliar().Player:ToPlayer() 
		end
		
		local pd
		if p then
			pd = p:GetData()
		end
		
		if not d.EU.raninit then
			
			t.FallingSpeed = t.FallingSpeed + 1.5
			t.Height = t.Height - 7.25
			local poof = Isaac.Spawn(1000, EffectVariant.TEAR_POOF_VERYSMALL, 0, t.Position, t.Velocity*0, nil):ToEffect()
			poof.SpriteOffset = Vector(0, math.ceil(t.Height/2 + 1))
			poof.SpriteScale = Vector(t.Scale, t.Scale)
			poof.DepthOffset = 5
			poof.Color = t.Color
			
			if pd then
				if pd.EU and pd.EU.Accuracy and type(pd.EU.Accuracy) == "number" then
					local tearRot = EU.randomfloat(-360 + pd.EU.Accuracy, 360 - pd.EU.Accuracy, EU.rng)
					t.Velocity = t.Velocity:Rotated(tearRot)
				end
				if pd.EU and pd.EU.epiphboost and type(pd.EU.epiphboost) == "number" then
					local tearRot = pd.EU.epiphboost * EU.randomfloat(-22.5, 22.5, EU.rng)
					t.Velocity = t.Velocity:Rotated(tearRot)
				end
			end
			
			if not d.EU.fauxtear and t.Parent and t.Parent.Type ~= t.Type then
				if p:HasCollectible(EU.CollectibleType.COLLECTIBLE_STRAWBERRYMILK) then
					local cnum = p:GetCollectibleNum(EU.CollectibleType.COLLECTIBLE_STRAWBERRYMILK, true)
					for i = 1, EU.random(3, 4, EU.rng) + (cnum*2) - 2 do
						local nt = EU.dupetear(t)
						nt.Velocity = nt.Velocity:Rotated(EU.random(-1000, 1000, EU.rng)/75)
						nt.Velocity = nt.Velocity * EU.random(750, 1250, EU.rng) /1000
						
						local tearmod = EU.random(9, 11, EU.rng)/10
						nt.CollisionDamage = nt.CollisionDamage * (tearmod^2)
						nt.Scale = nt.Scale * (tearmod)
						nt.FallingAcceleration = nt.FallingAcceleration * (tearmod^2)
						nt.FallingSpeed = nt.FallingSpeed * (tearmod^2)
						nt.Height = nt.Height * (tearmod^2)
					end
				end
			end
			t.Velocity = t.Velocity + t.Velocity:Normalized() * 1.25
			d.EU.raninit = true
		end
	end)
	
	
	EU:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, fx)
		local s = fx:GetSprite()
		local d = fx:GetData()
		fx:AddEntityFlags(EntityFlag.FLAG_PERSISTENT)
		if d.EU == nil then d.EU = {} end
		if d.EU.state == nil then d.EU.state = 0 end
		if d.EU.str == nil then d.EU.str = "|" end
		if d.EU.showtime == nil then d.EU.showtime = string.len(d.EU.str)*4 + 10 end
		if d.EU.state == 0 then
			s:Play("Appear")
			d.EU.state = 1
			fx.Velocity = fx.Velocity + Vector(4, 0)
		elseif d.EU.state == 1 then
			if s:IsFinished() and s:GetAnimation() == "Appear" then
				s:Play("Idle")
			end
			if s:GetAnimation() == "Idle" then
				if d.EU.showtime > 0 then 
					d.EU.showtime = d.EU.showtime - 1 
				else
					s:Play("Disappear")
					d.EU.state = 2
				end
			end
		elseif d.EU.state == 2 then
			if not EU.game:IsPaused() then
				fx.Velocity = fx.Velocity + Vector(-4, 0)
			end
			if s:IsFinished() and s:GetAnimation() == "Disappear" then
				fx:Remove()
			end
		end
		if not EU.game:IsPaused() then
			fx.Velocity = fx.Velocity - fx.Velocity/8
		end
	end, EU.EffectVariant.CHAT)
	
	
	
	EU:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, fx)
		local s = fx:GetSprite()
		local d = fx:GetData()
		if d.EU == nil then d.EU = {} end
		if s:GetAnimation() == "Appear" and s:IsFinished() then
			fx:Remove()
		end
	end, EU.EffectVariant.DASHPOOF)
	
	
	
	local deal, angel, devil, isduality
	EU:AddCallback(ModCallbacks.MC_POST_UPDATE, function(_)
		if DEALAPI then
			deal, angel, devil, isduality = DEALAPI:GetChance()
		else
			deal, angel, devil, isduality = 0
		end
	end)
	
	
	
	local ispaused = false
	local lastframe = 0
	local selector = 1
	EU:AddCallback(ModCallbacks.MC_GET_SHADER_PARAMS, function(_, shaderName)
		local p = EU.getPlayerBySlot(1)
		local menutrigger
		local menuconfirm = Input.IsActionTriggered(ButtonAction.ACTION_MENUCONFIRM, p.ControllerIndex)
		local menudir = {}
		if p.ControllerIndex == 0 then
			menutrigger = Input.IsActionTriggered(ButtonAction.ACTION_MENUBACK, p.ControllerIndex) or Input.IsActionTriggered(ButtonAction.ACTION_PAUSE, p.ControllerIndex)
			menudir = {
				u = Input.IsActionTriggered(ButtonAction.ACTION_UP, p.ControllerIndex),
				d = Input.IsActionTriggered(ButtonAction.ACTION_DOWN, p.ControllerIndex),
				l = Input.IsActionTriggered(ButtonAction.ACTION_LEFT, p.ControllerIndex),
				r = Input.IsActionTriggered(ButtonAction.ACTION_RIGHT, p.ControllerIndex)
			}
		else
			menutrigger = Input.IsActionTriggered(ButtonAction.ACTION_JOINMULTIPLAYER, p.ControllerIndex)
			menudir = {
				u = Input.IsActionTriggered(ButtonAction.ACTION_SHOOTUP, p.ControllerIndex),
				d = Input.IsActionTriggered(ButtonAction.ACTION_SHOOTDOWN, p.ControllerIndex) and not menuconfirm,
				l = Input.IsActionTriggered(ButtonAction.ACTION_SHOOTLEFT, p.ControllerIndex),
				r = Input.IsActionTriggered(ButtonAction.ACTION_SHOOTRIGHT, p.ControllerIndex) and not Input.IsActionTriggered(ButtonAction.ACTION_MENUBACK, p.ControllerIndex)
			}
		end
		if shaderName == "EUMENU" then
			EU.HOVec = Vector(EU.sd.s[1]*2, EU.sd.s[1]*1.25)
			local pausepos = EU.getScreenCenter() - Vector(145, 0)
			local optpos = pausepos - Vector(0, 70)
			
			if lastframe > 1 then lastframe = 0 end
			lastframe = lastframe + 1
			
			if EU.game:GetHUD():IsVisible() and EU.game:IsPaused() then
				if EU.game:IsPaused() and ispaused == false then
					if menutrigger then
						ispaused = true
						EU.menu.spr:Play("Appear", true)
						EU.sfx:Play(SoundEffect.SOUND_PAPER_IN, 1, 0, false, 1, 0)
					end
				elseif EU.game:IsPaused() and ispaused == true then
					if menutrigger or menuconfirm then
						ispaused = false
						EU.menu.spr:Play("Disappear", true)
						EU.sfx:Play(SoundEffect.SOUND_PAPER_OUT, 1, 0, false, 1, 0)
					end
				end
			end
			
			if EU.menu.spr:GetAnimation() == "Appear" and EU.menu.spr:IsFinished() then
				EU.menu.spr:Play("Idle", true)
			end
			if not EU.game:IsPaused() and ispaused == true then
				ispaused = false
			end
			if EU.game:IsPaused() and EU.game:GetHUD():IsVisible() then
				EU.menu.spr:Render(pausepos)
				if EU.menu.spr:GetAnimation() == "Idle" then
					if menudir.u then
						if selector > 1 then
							selector = selector - 1
						end
					end
					if menudir.d then
						if EU.menu.opt[selector + 1] then
							selector = selector + 1
						end
					end
					for i = 0, EU.len(EU.menu.opt) do
						if i > 0 then
							local ypos = optpos.Y + (28 * (i - 1))
							if selector == i then
								if menudir.l then
									if type(EU.sd.s[i]) == "number" then
										if EU.sd.s[i] > 0 then EU.sd.s[i] = EU.sd.s[i] - 1; EU.sfx:Play(SoundEffect.SOUND_PLOP, 1, 0, false, 1, 0) end
									elseif type(EU.sd.s[i]) == "boolean" then
										if EU.sd.s[i] == false then 
											EU.sd.s[i] = true
											EU.sfx:Play(SoundEffect.SOUND_PLOP, 1, 0, false, 1, 0)
										else
											EU.sd.s[i] = false
											EU.sfx:Play(SoundEffect.SOUND_PLOP, 1, 0, false, 1, 0)
										end
									end
								elseif menudir.r then
									if type(EU.sd.s[i]) == "number" then
										if EU.sd.s[i] < EU.menu.opt[i].maxi then EU.sd.s[i] = EU.sd.s[i] + 1; EU.sfx:Play(SoundEffect.SOUND_PLOP, 1, 0, false, 1, 0) end
									elseif type(EU.sd.s[i]) == "boolean" then
										if EU.sd.s[i] == false then 
											EU.sd.s[i] = true
											EU.sfx:Play(SoundEffect.SOUND_PLOP, 1, 0, false, 1, 0)
										else
											EU.sd.s[i] = false
											EU.sfx:Play(SoundEffect.SOUND_PLOP, 1, 0, false, 1, 0)
										end
									end
								end
							end
							local col
							if selector == i then col = KColor(1, 1, 1, 1) else col = KColor(1, 1, 1, 0.5) end
							EUfont[4]:DrawStringScaled(EU.menu.opt[i].name, optpos.X - EUfont[4]:GetStringWidth(EU.menu.opt[i].name)/2, ypos, 1, 1, col, 0, true)
							if type(EU.sd.s[i]) == "number" or type(EU.sd.s[i]) == "string" then
								local str = EU.sd.s[i] .. " / " .. EU.menu.opt[i].maxi
								EUfont[4]:DrawStringScaled(str, optpos.X - EUfont[4]:GetStringWidth(str)/2, ypos + 12, 1, 1, col, 0, true)
							else
								EUfont[4]:DrawStringScaled(tostring(EU.sd.s[i]), optpos.X - EUfont[4]:GetStringWidth(tostring(EU.sd.s[i]))/2, ypos + 12, 1, 1, col, 0, true)
							end
						end
					end
				end
				if lastframe == 1 then
					EU.menu.spr:Update()
				end
			end
		end
	end)

	
	
	EU:AddCallback(ModCallbacks.MC_POST_RENDER, function(_)
		EU.forceDoorTypes()
		EU.HOVec = Vector(EU.sd.s[1]*2, EU.sd.s[1]*1.25)
		local room = EU.game:GetRoom()
		for i = 0, (room:GetGridSize()) do
			local gent = room:GetGridEntity(i)
			if room:GetGridEntity(i) then
				if (gent.Desc.Type == 16) then
					local Door = gent:ToDoor()
					local s = Door:GetSprite()
					if s:GetFilename() == "gfx/grid/stagedoors/door_darkroom.anm2" then
						if s:GetAnimation() == "Open" and s:IsFinished() then
							s:Play("Opened", true)
						end
					elseif s:GetFilename() == "gfx/grid/stagedoors/door_womb.anm2" then
						if s:GetAnimation() == "Open" and s:IsFinished() then
							s:Play("Opened", true)
						end
						if s:GetAnimation() == "Close" and s:IsFinished() then
							s:Play("Closed", true)
						end
					end
				end
			end
		end
		for i, e in pairs(Isaac.FindByType(EntityType.ENTITY_TEAR, -1, -1, false, false)) do; local t = e:ToTear();
			local d = t:GetData()
			if not d.EU then d.EU = {} end
			--[[if not d.EU.raninit then
				t.Velocity = t.Velocity + t.Velocity:Normalized() * 1.25
				t.FallingSpeed = t.FallingSpeed + 1.5
				t.Height = t.Height - 7.25
				local poof = Isaac.Spawn(1000, EffectVariant.TEAR_POOF_VERYSMALL, 0, t.Position + t.Velocity, t.Velocity*0, p):ToEffect()
				poof.SpriteOffset = Vector(0, t.Height/1.5 + 1)
				poof.SpriteScale = Vector(t.Scale, t.Scale)
				poof.DepthOffset = 5
				poof.Color = t.Color
				d.EU.raninit = true
			end]]
		end
		for i, e in pairs(Isaac.FindByType(EntityType.ENTITY_EFFECT, EU.EffectVariant.CHAT, -1, false, false)) do; local fx = e:ToEffect();
			local d = fx:GetData()
			if d.EU == nil then d.EU = {} end
			if d.EU.str then
				local pos = Isaac.WorldToRenderPosition(fx.Position)
				EUfont[3]:DrawString(d.EU.str, pos.X, pos.Y, KColor(1, 1, 1, 1), 0, true)
			end
		end
		for i, e in pairs(Isaac.FindByType(EntityType.ENTITY_PROJECTILE, -1, -1, false, false)) do; local pr = e:ToProjectile();
			local d = pr:GetData()
			if not d.EU then d.EU = {} end
			if not d.EU.raninit then
				local level = EU.game:GetLevel()
				local stagenum = level:GetStage()
				local modifier = 8
				if stagenum / modifier < 1 then
					pr.Velocity = pr.Velocity + pr.Velocity:Normalized() * 1 
				else
					pr.Velocity = pr.Velocity + pr.Velocity:Normalized() * stagenum / modifier
				end
				d.EU.raninit = true
			end
		end
		for i = 0, EU.game:GetNumPlayers() - 1 do
			local p = Isaac.GetPlayer(i)
			local lastp = Isaac.GetPlayer(i - 1)
			if not p.Parent and EU.game:GetHUD():IsVisible() then
				local d = p:GetData()
				local theme = EU.hudthemes[d.EU.hudtheme]
				if d.EU.name == nil then d.EU.name = p:GetName() end
				if d.EU.playerSlot == nil then d.EU.playerSlot = 1 end
				if p.ControllerIndex == lastp.ControllerIndex then
					if d.EU.name ~= lastp:GetData().EU.name and (lastp:GetData().EU.name ~= lastp:GetName()) then
						d.EU.name = lastp:GetData().EU.name
					end
					if lastp:GetData().EU.playerSlot then
						d.EU.playerSlot = lastp:GetData().EU.playerSlot
					end
				else
					if d.EU and d.EU.playerSlot and d.EU.playerSlot == 1 then
						d.EU.playerSlot = lastp:GetData().EU.playerSlot + 1
					end
				end
				if d.EU.renderalpha == nil then d.EU.renderalpha = 0 end
				local alph = d.EU.renderalpha/100
				local pos = Isaac.WorldToScreen(p.Position - Vector(0, 4) + p.SpriteOffset) 
				local tps = 30 / (p.MaxFireDelay + 1)
				local acc
				if d.EU and d.EU.Accuracy then
					acc = EU.round((d.EU.Accuracy^3/(360^3))*100, 10)
				else
					acc = 100
				end
				local numstats = 6
				local statpos = Vector(pos.X + 24, pos.Y - 44)
				if p:GetPlayerType() == PlayerType.PLAYER_ESAU or p:GetPlayerType() == PlayerType.PLAYER_THEFORGOTTEN_B then
					if d.EU.hudtheme == nil then
						d.EU.hudtheme = -1
					end
				else
					if d.EU.hudtheme == nil then d.EU.hudtheme = 0 end
				end
				
				if p:GetPlayerType() ~= PlayerType.PLAYER_ESAU and p:GetPlayerType() ~= PlayerType.PLAYER_JACOB then
					if Input.IsActionPressed(ButtonAction.ACTION_DROP, p.ControllerIndex) or Input.IsActionPressed(ButtonAction.ACTION_MAP, p.ControllerIndex) or Input.IsButtonPressed(Keyboard.KEY_LEFT_SHIFT, p.ControllerIndex) then
						if d.EU.renderalpha < 50 then d.EU.renderalpha = d.EU.renderalpha + 10 end
					else
						if d.EU.renderalpha > 0 then d.EU.renderalpha = d.EU.renderalpha - 1 end
					end
				elseif p:GetPlayerType() == PlayerType.PLAYER_ESAU then
					if Input.IsActionPressed(ButtonAction.ACTION_MAP, p.ControllerIndex) or Input.IsButtonPressed(Keyboard.KEY_LEFT_SHIFT, p.ControllerIndex) then
						if d.EU.renderalpha < 50 then d.EU.renderalpha = d.EU.renderalpha + 10 end
					else
						if d.EU.renderalpha > 0 then d.EU.renderalpha = d.EU.renderalpha - 1 end
					end
				elseif p:GetPlayerType() == PlayerType.PLAYER_JACOB then
					if Input.IsActionPressed(ButtonAction.ACTION_DROP, p.ControllerIndex) or Input.IsButtonPressed(Keyboard.KEY_LEFT_SHIFT, p.ControllerIndex) then
						if d.EU.renderalpha < 50 then d.EU.renderalpha = d.EU.renderalpha + 10 end
					else
						if d.EU.renderalpha > 0 then d.EU.renderalpha = d.EU.renderalpha - 1 end
					end
				end
				
				if d.EU and d.EU.playerSlot and EU.sd.s[2] ~= 0 and not EU.game:IsPaused() then
					if alph > 0 then
						if theme and theme.Type == 0 then
							local hcol = KColor(theme.overhead.r, theme.overhead.g, theme.overhead.b, alph*2)
							local subhcol = KColor(theme.overhead.r, theme.overhead.g, theme.overhead.b, alph/2)
							if not EU.isSinglePlayer() then
								EUfont[1]:DrawStringScaled(d.EU.playerSlot, pos.X - 5, pos.Y - 58, 1, 1, hcol, 0, true)
								if d.EU.ControllerName then
									EUfont[3]:DrawStringScaled(string.upper(d.EU.name), pos.X + 24, pos.Y - 64, 1, 1, subhcol, 0, true)
									EUfont[3]:DrawStringScaled(d.EU.ControllerName, pos.X + 24, pos.Y - 54, 1, 1, hcol, 0, true)
								else
									EUfont[3]:DrawStringScaled(string.upper(d.EU.name), pos.X + 24, pos.Y - 54, 1, 1, hcol, 0, true)
								end
							else
								EUfont[3]:DrawStringScaled(string.upper(d.EU.name), pos.X - 17, pos.Y - 54, 1, 1, hcol, 0, true)
							end
							if EU.sd.s[2] == 2 then
								EUfont[2]:DrawStringScaled("_", pos.X - 20, pos.Y - 46, 28, 1, hcol, 0, true)
							else
								EUfont[2]:DrawStringScaled("_", pos.X - 20, pos.Y - 46, 17, 1, hcol, 0, true)
							end
							for i = 0, numstats do
								local col = KColor(theme[i].r, theme[i].g, theme[i].b, alph)
								local devcol = KColor(theme.dev.r, theme.dev.g, theme.dev.b, alph/2)
								local angcol = KColor(theme.ang.r, theme.ang.g, theme.ang.b, alph/2)
								if i == 0 then
									EUfont[0]:DrawStringScaled("SPD", statpos.X, statpos.Y + (7 * i), 1, 1, col, 0, true)
									EUfont[0]:DrawStringScaled("| " .. EU.round(p.MoveSpeed, 100), statpos.X + 30, statpos.Y + (7 * i), 1, 1, col, 0, true)
								elseif i == 1 then
									EUfont[0]:DrawStringScaled("TPS", statpos.X, statpos.Y + (7 * i), 1, 1, col, 0, true)
									EUfont[0]:DrawStringScaled("| " .. EU.round(tps, 100), statpos.X + 30, statpos.Y + (7 * i), 1, 1, col, 0, true)
									if EU.sd.s[2] == 2 then
										EUfont[0]:DrawStringScaled("| Fire Delay", statpos.X + 64 * 0.9, statpos.Y + (7 * 1.5 * i), 0.5, 0.5, col, 0, true)
										EUfont[0]:DrawStringScaled("| " .. EU.round((p.FireDelay + 1)/(p.MaxFireDelay), 100)*100 .. "%", statpos.X + 118*0.85, statpos.Y + (7 * 1.5 * i), 0.5, 0.5, col, 0, true)
										EUfont[0]:DrawStringScaled("| Tears", statpos.X + 64 * 0.9, statpos.Y + (7 * 2 * i), 0.5, 0.5, col, 0, true)
										EUfont[0]:DrawStringScaled("| " .. EU.round(p.MaxFireDelay, 100), statpos.X + 118*0.85, statpos.Y + (7 * 2 * i), 0.5, 0.5, col, 0, true)
									end
								elseif i == 2 then
									EUfont[0]:DrawStringScaled("DMG", statpos.X, statpos.Y + (7 * i), 1, 1, col, 0, true)
									EUfont[0]:DrawStringScaled("| " .. EU.round(p.Damage, 100), statpos.X + 30, statpos.Y + (7 * i), 1, 1, col, 0, true)
									if EU.sd.s[2] == 2 then
										EUfont[0]:DrawStringScaled("| DPS", statpos.X + 64 * 0.9, statpos.Y + (7 * 1.25 * i), 0.5, 0.5, col, 0, true)
										EUfont[0]:DrawStringScaled("| " .. EU.round(tps * p.Damage, 100), statpos.X + 118*0.85, statpos.Y + (7 * 1.25 * i), 0.5, 0.5, col, 0, true)
									end
								elseif i == 3 then
									EUfont[0]:DrawStringScaled("RANGE", statpos.X, statpos.Y + (7 * i), 1, 1, col, 0, true)
									EUfont[0]:DrawStringScaled("| " .. "NaN", statpos.X + 30, statpos.Y + (7 * i), 1, 1, col, 0, true)
								elseif i == 4 then
									EUfont[0]:DrawStringScaled("SHSPD", statpos.X, statpos.Y + (7 * i), 1, 1, col, 0, true)
									EUfont[0]:DrawStringScaled("| " .. EU.round(p.ShotSpeed, 100), statpos.X + 30, statpos.Y + (7 * i), 1, 1, col, 0, true)
								elseif i == 5 then
									EUfont[0]:DrawStringScaled("LUCK", statpos.X, statpos.Y + (7 * i), 1, 1, col, 0, true)
									EUfont[0]:DrawStringScaled("| " .. EU.round(p.Luck, 100), statpos.X + 30, statpos.Y + (7 * i), 1, 1, col, 0, true)
								elseif i == 6 then
									EUfont[0]:DrawStringScaled("ACC", statpos.X, statpos.Y + (7 * i), 1, 1, col, 0, true)
									EUfont[0]:DrawStringScaled("| " .. acc .. "%", statpos.X + 30, statpos.Y + (7 * i), 1, 1, col, 0, true)
								end
								if i == numstats then
									if angel and devil and deal then
										EUfont[0]:DrawStringScaled("DEVIL", statpos.X, statpos.Y + (7 * (i + 1)), 1, 1, devcol, 0, true)
										EUfont[0]:DrawStringScaled("| " .. EU.round(devil*deal*100, 10) .. "%", statpos.X + 30, statpos.Y + (7 * (i + 1)), 1, 1, devcol, 0, true)
										EUfont[0]:DrawStringScaled("ANGEL", statpos.X, statpos.Y + (7 * (i + 2)), 1, 1, angcol, 0, true)
										EUfont[0]:DrawStringScaled("| " .. EU.round(angel*deal*100, 10) .. "%", statpos.X + 30, statpos.Y + (7 * (i + 2)), 1, 1, angcol, 0, true)
									end
								end
							end
						end
						--insert new themetype
					else
						if theme then
							local hcol = KColor(theme.overhead.r, theme.overhead.g, theme.overhead.b, (0.5 - alph)/2)
							if not EU.isSinglePlayer() and not EU.game:IsPaused() then
								if d.EU.ControllerName then
									EUfont[0]:DrawStringScaled(d.EU.ControllerName, pos.X - EUfont[0]:GetStringWidth(d.EU.ControllerName)/2, pos.Y - 48, 1, 1, hcol, 0, true)
								end
							end
						end
					end
				end
				if d.EU == nil then d.EU = {} end
				if d.EU.GlassHearts and d.EU.GlassHearts > 0 then
					local heartpos = Vector(0, 0)
					local heartcount = p:GetMaxHearts() + p:GetSoulHearts() + (p:GetBoneHearts() * 2) + (p:GetBrokenHearts() * 2)
					if i == 0 then
						if p:GetPlayerType() == PlayerType.PLAYER_THELOST or p:GetPlayerType() == PlayerType.PLAYER_THELOST_B then
							heartpos = Vector(28, 4) + EU.HOVec
						else
							heartpos = Vector(28, 4 + 10 * math.ceil(heartcount/12)) + EU.HOVec
						end
						EU.renderGlassHearts(d.EU.GlassHearts, heartpos)
					elseif i == 1 then
						if p:GetPlayerType() == PlayerType.PLAYER_THELOST or p:GetPlayerType() == PlayerType.PLAYER_THELOST_B then
							heartpos = EU.getScreenTR() - Vector(131, - (-6 + 10 * math.ceil(heartcount/6))) + Vector(-EU.HOVec.X, EU.HOVec.Y)
						else
							heartpos = EU.getScreenTR() - Vector(131, - (4 + 10 * math.ceil(heartcount/6))) + Vector(-EU.HOVec.X, EU.HOVec.Y)
						end
						if d.EU.GlassHearts < 4 then
							EU.renderGlassHearts(d.EU.GlassHearts, heartpos)
						else
							EU.renderGlassHearts(3, heartpos)
							EU.renderGlassHearts(d.EU.GlassHearts - 3, heartpos + Vector(0, 10))
						end
					elseif i == 2 then
						if p:GetPlayerType() == PlayerType.PLAYER_THELOST or p:GetPlayerType() == PlayerType.PLAYER_THELOST_B then
							heartpos = EU.getScreenBL() + Vector(74 - 12*3, -35) + Vector(EU.HOVec.X, -EU.HOVec.Y)
						else
							heartpos = EU.getScreenBL() + Vector(74, -35) + Vector(EU.HOVec.X, -EU.HOVec.Y)
						end
						if d.EU.GlassHearts < 4 then
							EU.renderGlassHearts(d.EU.GlassHearts, heartpos)
						else
							EU.renderGlassHearts(3, heartpos)
							EU.renderGlassHearts(d.EU.GlassHearts - 3, heartpos + Vector(0, 10))
						end
					elseif i == 3 then
						if p:GetPlayerType() == PlayerType.PLAYER_THELOST or p:GetPlayerType() == PlayerType.PLAYER_THELOST_B then
							heartpos = EU.getScreenBR() - Vector(74 + 29 + 12*3, 35) + Vector(-EU.HOVec.X, -EU.HOVec.Y)
						else
							heartpos = EU.getScreenBR() - Vector(74 + 29, 35) + Vector(-EU.HOVec.X, -EU.HOVec.Y)
						end
						if d.EU.GlassHearts < 4 then
							EU.renderGlassHearts(d.EU.GlassHearts, heartpos)
						else
							EU.renderGlassHearts(3, heartpos)
							EU.renderGlassHearts(d.EU.GlassHearts - 3, heartpos + Vector(0, 10))
						end
					end
				end
			end
		end
	end)
	
	
	
	EU:AddCallback(ModCallbacks.MC_PRE_TEAR_COLLISION, function(_, t, e, low)
		if e:GetEntityFlags() & EntityFlag.FLAG_NO_PHYSICS_KNOCKBACK > 0 then
		--	e.Velocity = e.Velocity + t.Velocity / 1.5
		end
	end)
	
	
	
	EU:AddCallback(ModCallbacks.MC_POST_NPC_DEATH, function(_, e)
		if e.Variant == 5 then
			local s = e:GetSprite()
			local anim = s:GetAnimation()
			if anim == "FlyLit" then
				if EU.random(1, 3, EU.rng) == 1 then
					if EU.random(1, 3, EU.rng) == 1 then
						Isaac.Spawn(5, PickupVariant.PICKUP_LIL_BATTERY, BatterySubType.BATTERY_NORMAL, e.Position, e.Velocity, e)
					else
						Isaac.Spawn(5, PickupVariant.PICKUP_LIL_BATTERY, BatterySubType.BATTERY_MICRO, e.Position, e.Velocity, e)
					end
				end
			end
		end
	end, 61)
	
	
	
	--[[EU:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, e)
		--EntityFlag.FLAG_FRIENDLY | EntityFlag.FLAG_CHARM | EntityFlag.FLAG_PERSISTENT
		local flags = e:GetEntityFlags()
		if flags & EntityFlag.FLAG_FRIENDLY > 0 and flags & EntityFlag.FLAG_CHARM > 0 and flags & EntityFlag.FLAG_PERSISTENT > 0 then
		end
	end)]]
	
	
	
	EU:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function(_)
		for i, e in ipairs(Isaac.GetRoomEntities()) do
			local d = e:GetData()
			local flags = e:GetEntityFlags()
			if flags & EntityFlag.FLAG_FRIENDLY > 0 and flags & EntityFlag.FLAG_CHARM > 0 and flags & EntityFlag.FLAG_PERSISTENT > 0 then
				if d.CharmParent then
					e.Position = d.CharmParent.Position
				end
			end
		end
	end)
	
	
	
	EU:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(_, f)
		local p = f.Player:ToPlayer()
		if (f.Position - p.Position):Length() > 125 then
		--	p.Velocity = p.Velocity - (p.Position - f.Position):Resized((f.Position - p.Position):Length()/200)
		end
	end, 900)
	
	

	EU:AddCallback(ModCallbacks.MC_POST_EFFECT_INIT, function(_, fx)
		EU.game:ShakeScreen(5*fx.Scale)
	end, EffectVariant.BOMB_EXPLOSION)
	
	EU:AddCallback(ModCallbacks.MC_POST_ENTITY_KILL, function(_, e)
		if e.Type == 1 then
			local p = e:ToPlayer()
			local d = p:GetData()
			
			if d.EU.IsKillable == false then
				p:Revive()
				if p:GetMaxHearts() == 0 then
					p:AddSoulHearts(-1)
				else
					p:AddHearts(-1)
				end
				if EU.sfx:IsPlaying(30) then
					EU.sfx:Stop(30)
				end
			end
			
			local d = p:GetData()
			if d.EU.GlassHearts > 0 then
				p:Revive()
				if p:GetMaxHearts() == 0 then
					p:AddSoulHearts(-1)
				end
				if EU.sfx:IsPlaying(30) then
					EU.sfx:Stop(30)
				end
			end
		end
	end)
	
	EU:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, e, amount, flags, source, countdown)
		if e.Type == EntityType.ENTITY_PLAYER then
			local p = e:ToPlayer()
			if flags & DamageFlag.DAMAGE_CLONES < 1 then
				if flags & DamageFlag.DAMAGE_NOKILL > 0 and flags & DamageFlag.DAMAGE_FAKE > 0 then
				else
					if p:GetEffects():HasNullEffect(NullItemID.ID_LOST_CURSE) == false then
						p:TakeDamage(amount, flags | DamageFlag.DAMAGE_CLONES, source, countdown)
						p:SetMinDamageCooldown(countdown)
						return false
					end
				end
			end
			local playerseed = p:GetCollectibleRNG(0):GetSeed()
			local s = p:GetSprite()
			local d = p:GetData()
			if d.EU == nil then d.EU = {} end
			if d.EU.epiphboost == nil then d.EU.epiphboost = 0 end
			if d.EU.epiphboost and d.EU.epiphboost > 0 or d.EU.epiphboost < 0 then 
				d.EU.epiphboost = 0 
				p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
				p:EvaluateItems()
			end
			if d.EU.GlassHearts > 0 and amount > 0 then
				p:UseActiveItem(486, false, false, false, false, ActiveSlot.SLOT_PRIMARY)
				d.EU.GlassHearts = d.EU.GlassHearts - 1
				p:SetMinDamageCooldown(80)
				EU.sfx:Play(SoundEffect.SOUND_GLASS_BREAK, 1, 0, false, 1, 0)
				p:UseActiveItem(38, false, false, false, false, ActiveSlot.SLOT_PRIMARY)
				return false
			end
			if EU.sd.cruciblecharge[tostring(playerseed)] and EU.sd.cruciblecharge[tostring(playerseed)] > 0 and amount > 0 then
				p:UseActiveItem(486, false, false, false, false, ActiveSlot.SLOT_PRIMARY)
				EU.sfx:Play(SoundEffect.SOUND_HOLY_MANTLE, 0.25, 0, false, 1.5, 0)
				EU.sd.cruciblecharge[tostring(playerseed)] = EU.sd.cruciblecharge[tostring(playerseed)] - 1
				return false
			end
		end
	end)
	
	
	
	EU:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, function(_, pi)
		local s = pi:GetSprite()
		if (s:WasEventTriggered("DropSound") or s:WasEventTriggered("Drop")) and s:GetAnimation() == "Appear" or s:GetAnimation() ~= "Collect" then
			pi:MultiplyFriction(0.9)
		end
		if s:GetAnimation() == "Appear" and not (s:WasEventTriggered("DropSound") or s:WasEventTriggered("Drop")) then
			s.PlaybackSpeed = 1.75
		else
			s.PlaybackSpeed = 0.9
		end
		if pi.Variant == PickupVariant.PICKUP_GRAB_BAG then
			if pi.SubType == 69 then -- big grab bag
				if s:GetAnimation() == "Collect" then 
					pi.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE 
				end
				if s:GetAnimation() == "Collect" and s:IsFinished() then pi:Remove() end
				if s:IsEventTriggered("DropSound") then
					EU.sfx:Play(38, 1, 0, false, 1, 0)
				end
			elseif pi.SubType == 70 then -- holy sack
				if s:GetAnimation() == "Collect" then 
					pi.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE 
				end
				if s:GetAnimation() == "Collect" and s:IsFinished() then pi:Remove() end
				if s:IsEventTriggered("DropSound") then
					EU.sfx:Play(38, 1, 0, false, 1, 0)
				end
			elseif pi.SubType == 1 then -- normal sack
				if s:GetAnimation() == "Appear" and s:GetFrame() == 1 then
					if EU.random(1, 10, EU.rng) == 1 then
						pi:Morph(pi.Type, pi.Variant, 69, true, true, false)
					end
				end
			end
		elseif pi.Variant == PickupVariant.PICKUP_HEART then
			if pi.SubType == 69 then
				if s:GetAnimation() == "Collect" then
					pi.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE 
				end
				if s:GetAnimation() == "Collect" and s:IsFinished() then pi:Remove() end
				if s:IsEventTriggered("DropSound") then
					EU.sfx:Play(SoundEffect.SOUND_KEY_DROP0, 2, 0, false, 2, 0)
				end
			end
		end
	end)



	EU:AddCallback(ModCallbacks.MC_PRE_PICKUP_COLLISION, function(_, pi, e)
		if e and e.Type == EntityType.ENTITY_PLAYER then
			local p = e:ToPlayer()
			local d = p:GetData()
			local pi_s = pi:GetSprite()
			if pi.Variant == PickupVariant.PICKUP_GRAB_BAG and (pi_s:GetAnimation() == "Idle" or (pi_s:GetAnimation() == "Appear") and pi_s:WasEventTriggered("DropSound")) then
				if pi.SubType == 69 then
					local numpickups = EU.random(8, 10, EU.rng)
					local var = -1
					for i = 1, numpickups do
						local chance = EU.random(1, 6, EU.rng)
						if chance == 1 then
							var = PickupVariant.PICKUP_COIN
						elseif chance == 2 then
							var = PickupVariant.PICKUP_COIN
						elseif chance == 3 then
							var = PickupVariant.PICKUP_KEY
						elseif chance == 4 then
							var = PickupVariant.PICKUP_BOMB
						elseif chance == 5 then
							if EU.random(1, 3, EU.rng) == 1 then
								var = PickupVariant.PICKUP_COIN
							else
								var = PickupVariant.PICKUP_TAROTCARD
							end
						elseif chance == 6 then
							if EU.random(1, 3, EU.rng) == 1 then
								var = PickupVariant.PICKUP_COIN
							else
								var = PickupVariant.PICKUP_LIL_BATTERY
							end
						end
						Isaac.Spawn(5, var, 0, pi.Position, EU.getrandomunitvec():Resized(EU.randomfloat(4, 6, EU.rng)), pi)
					end
					EU.sfx:Play(252, 1, 0, false, 1, 0)
					pi_s:Play("Collect", true)
					pi.Touched = true
					return true
				elseif pi.SubType == 70 then
					local numpickups = EU.random(2, 4, EU.rng)
					local subtype = -1
					for i = 1, numpickups do
						local chance = EU.random(1, 4, EU.rng)
						if chance == 1 then
							subtype = HeartSubType.HEART_ETERNAL
						elseif chance == 2 then
							subtype = 69
						else
							subtype = HeartSubType.HEART_SOUL
						end
						Isaac.Spawn(5, PickupVariant.PICKUP_HEART, subtype, pi.Position, EU.getrandomunitvec():Resized(EU.randomfloat(4, 6, EU.rng)), pi)
					end
					EU.sfx:Play(252, 1, 0, false, 1, 0)
					pi_s:Play("Collect", true)
					pi.Touched = true
					return true
				end
			elseif pi.Variant == PickupVariant.PICKUP_HEART and pi.SubType == 69 and (pi_s:GetAnimation() == "Idle" or pi_s:GetAnimation() == "Appear" and pi_s:WasEventTriggered("DropSound")) then
				if d.EU == nil then d.EU = {} end
				if d.EU.GlassHearts and d.EU.GlassHearts < 6 then
					pi_s:Play("Collect", true)
					pi.Touched = true
					EU.sfx:Play(SoundEffect.SOUND_POT_BREAK_2, 0.75, 0, false, 2.75, 0)
					d.EU.GlassHearts = d.EU.GlassHearts + 1
					return true
				end
			end
		end
	end, e, -1)
	
	
	
	EU:AddCallback(ModCallbacks.MC_PRE_SPAWN_CLEAN_AWARD, function(_, rng, pos)
		local room = EU.game:GetRoom()
		if room:GetType() ~= RoomType.ROOM_BOSS and EU.random(1, 30, rng) == 1 then
			Isaac.Spawn(5, PickupVariant.PICKUP_HEART, 69, Isaac.GetFreeNearPosition(pos, 1), Vector(0, 0), nil)
			return true
		end
	end)
	
	
	
	EU:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
		local d = p:GetData()
		if d.EU and d.EU.GlassHearts and d.EU.GlassHearts < 6 then
			d.EU.GlassHearts = d.EU.GlassHearts + 1
			EU.sfx:Play(SoundEffect.SOUND_POT_BREAK_2, 0.75, 0, false, 2.75, 0)
			return true
		end
	end, EU.CollectibleType.COLLECTIBLE_FRAGILEHEART)
	
	
	
	EU:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
		local playerseed = p:GetCollectibleRNG(0):GetSeed()
		if id == EU.CollectibleType.COLLECTIBLE_CRICKETSPAW then
			if p:GetMaxHearts() > 2 then
				EU.sd.cricketpawboost[tostring(playerseed)] = EU.sd.cricketpawboost[tostring(playerseed)] + 0.5
				EU.sfx:Play(SoundEffect.SOUND_VAMP_GULP, 1, 0, false, 1, 0)
				p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
				p:EvaluateItems()
				p:AddMaxHearts(-2)
				return true
			end
		elseif id == EU.CollectibleType.COLLECTIBLE_OCCAMSRAZOR then
			if p:GetBrokenHearts() < 11 then
				if not EU.sd.occamboost then EU.sd.occamboost = {} end
				if not EU.sd.occamboost[tostring(playerseed)] then
					EU.sd.occamboost[tostring(playerseed)] = 0
				end
				EU.sd.occamboost[tostring(playerseed)] = EU.sd.occamboost[tostring(playerseed)] + 1
				p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
				p:AddCacheFlags(CacheFlag.CACHE_SPEED)
				p:AddCacheFlags(CacheFlag.CACHE_SHOTSPEED)
				p:AddCacheFlags(CacheFlag.CACHE_LUCK)
				p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
				p:EvaluateItems()
				p:UseActiveItem(486, false, false, false, false, ActiveSlot.SLOT_PRIMARY)
				p:AddBrokenHearts(1)
				return true
			end
		elseif id == EU.CollectibleType.COLLECTIBLE_BOOKOFLOVE then
			--SoundEffect.SOUND_BOOK_PAGE_TURN_12
			for i, e in ipairs(Isaac.GetRoomEntities()) do
				if (e:IsEnemy() and e:IsActiveEnemy(false) and e:IsVulnerableEnemy()) and e:IsBoss() == false then
					if (p.Position - e.Position):Length() <= 120 then
						local edata = e:GetData()
						edata.CharmParent = p
						if e.CollisionDamage <= 0 then
							e.CollisionDamage = 1
						end
						e:AddEntityFlags(EntityFlag.FLAG_FRIENDLY | EntityFlag.FLAG_CHARM | EntityFlag.FLAG_PERSISTENT)
					end
				end
			end
			local fx =Isaac.Spawn(1000, 16, 1, p.Position, p.Velocity*0, p):ToEffect()
			fx.Parent = p
			fx.Color = Color(1, 0.6, 0.9, 1, 0, 0, 0)
			EU.sfx:Play(SoundEffect.SOUND_BOOK_PAGE_TURN_12, 1, 0, false, 1, 0)
			return true
		elseif id == EU.CollectibleType.COLLECTIBLE_CRUCIBLE then
			--cruciblecharge
			if EU.sd.cruciblecharge[tostring(playerseed)] == nil then EU.sd.cruciblecharge[tostring(playerseed)] = 0 end
			if p:GetSoulHearts() >= 2 then
				p:AddSoulHearts(-2)
				EU.sfx:Play(SoundEffect.SOUND_URN_OPEN, 1, 0, false, 1.25, 0)
				EU.sd.cruciblecharge[tostring(playerseed)] = EU.sd.cruciblecharge[tostring(playerseed)] + 2
				return true
			end
		end
	end)
	
	
	
	EU:AddCallback(ModCallbacks.MC_EXECUTE_CMD, function(_, cmd, params)
		if cmd == "leave" then
			EU.game:Fadeout(1, 1)
		else
			local str = tonumber(cmd)
			for i, e in pairs(Isaac.FindByType(EntityType.ENTITY_PLAYER, -1, -1, false, false)) do; local p = e:ToPlayer();
				local d = p:GetData()
				if d.EU and d.EU.playerSlot == str then
					local txt = Isaac.Spawn(1000, EU.EffectVariant.CHAT, 0, EU.getScreenBL()*1.25 + (Vector(0, 12) * d.EU.playerSlot), Vector(0, 0), p):ToEffect()
					local txtd = txt:GetData()
					if txtd.EU == nil then txtd.EU = {} end
					txtd.EU.str = "<"..d.EU.ControllerName.."> "..params
				end
			end
		end
	end)
	
	
	
	function EU:newRoom_newLevel()
		EU.forceDoorTypes()
	end
	EU:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, EU.newRoom_newLevel)
	EU:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, EU.newRoom_newLevel)
	
	
	
	do -- laz rework
		EU:AddCallback(ModCallbacks.MC_USE_PILL, function(_, pillEffect, p, flag)
			--if flag & UseFlag.USE_MIMIC == 0 then
				--p:UsePill(pillEffect, p:GetPill(0), UseFlag.USE_MIMIC)
			--end
			local playerseed = p:GetCollectibleRNG(0):GetSeed()
			if p:GetPlayerType() == PlayerType.PLAYER_LAZARUS or p:GetPlayerType() == PlayerType.PLAYER_LAZARUS_B then
				if p:GetHearts() >= p:GetEffectiveMaxHearts() then
					if p:GetSoulHearts() < 2 then
						p:AddSoulHearts(1)
						EU.sfx:Play(SoundEffect.SOUND_VAMP_GULP, 1, 0, false, 1, 0)
					end
				else
					p:AddHearts(1)
					EU.sfx:Play(SoundEffect.SOUND_VAMP_GULP, 1, 0, false, 1, 0)
				end
			elseif p:GetPlayerType() == PlayerType.PLAYER_LAZARUS2 or p:GetPlayerType() == PlayerType.PLAYER_LAZARUS2_B then
				p:AddBrokenHearts(1)
				p:UseActiveItem(486, false, false, false, false, ActiveSlot.SLOT_PRIMARY)
				EU.sfx:Play(SoundEffect.SOUND_VAMP_DOUBLE, 1, 0, false, 1, 0)
				if not EU.sd.lazIIboost then EU.sd.lazIIboost = {} end
				if not EU.sd.lazIIboost[tostring(playerseed)] then
					EU.sd.lazIIboost[tostring(playerseed)] = 0
				end
				EU.sd.lazIIboost[tostring(playerseed)] = EU.sd.lazIIboost[tostring(playerseed)] + 1
				p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
				p:AddCacheFlags(CacheFlag.CACHE_SPEED)
				p:AddCacheFlags(CacheFlag.CACHE_SHOTSPEED)
				p:AddCacheFlags(CacheFlag.CACHE_LUCK)
				p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
				p:EvaluateItems()
				
			end
		end)
	end
	
	
	EU:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, effect)
		if effect.Parent and effect.Child and effect:GetData().NumOfElements then
			local data=effect:GetData()
			local total,current=data.NumOfElements,data.BeadNum
			if current==total then
				effect.Visible=false
			end
			local effectendpos=effect.Parent.Position+((effect.Child.Position+effect.Child.Velocity*2)-effect.Parent.Position)*current/total
			effect.Velocity = (effectendpos - effect.Position + (effect.Parent.Velocity*2 / current))
			effect.SpriteOffset=Vector(0,-6+effect.Child.SpriteOffset.Y*current/total+effect.Parent.SpriteOffset.Y*(1-current/total))
			effect.DepthOffset = -5
		elseif not effect.Child or effect.Child:IsDead() then
			effect:Remove()
		end
	end, 37916)
	
	
	
	EU:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, p, hook, action)
		if p ~= nil and p.Type == EntityType.ENTITY_PLAYER and not p:IsDead() then
			local p = p:ToPlayer()
			local s = p:GetSprite()
			local d = p:GetData()
			if p:GetPlayerType() == EU.PlayerType.PLAYER_JACOB_E then
				if hook == 2 then
					if action == ButtonAction.ACTION_LEFT then
						return 0
					end
					if action == ButtonAction.ACTION_RIGHT then
						return 0
					end
					if action == ButtonAction.ACTION_UP then
						return 0
					end
					if action == ButtonAction.ACTION_DOWN then
						return 0
					end
				elseif hook == 0 then
					if action == ButtonAction.ACTION_SHOOTLEFT then
						return false
					end
					if action == ButtonAction.ACTION_SHOOTRIGHT then
						return false
					end
					if action == ButtonAction.ACTION_SHOOTUP then
						return false
					end
					if action == ButtonAction.ACTION_SHOOTDOWN then
						return false
					end
				end
			end
		end
	end)

	
	
	EU:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, function(_, pi)
		local s = pi:GetSprite()
		local isred = (pi.SubType == HeartSubType.HEART_FULL or pi.SubType == HeartSubType.HEART_HALF or pi.SubType == HeartSubType.HEART_DOUBLEPACK)
		if isred and pi.Variant == PickupVariant.PICKUP_HEART then
			if s:GetAnimation() == "Appear" and s:GetFrame() == 1 then
				local islaz = islaz or false
				for i = 0, EU.game:GetNumPlayers() - 1 do
					p = Isaac.GetPlayer(i)
					if p:GetPlayerType() == PlayerType.PLAYER_LAZARUS or p:GetPlayerType() == PlayerType.PLAYER_LAZARUS2 or p:GetPlayerType() == PlayerType.PLAYER_LAZARUS_B or p:GetPlayerType() == PlayerType.PLAYER_LAZARUS2_B then
						islaz = true
					end
				end
				if EU.random(1, 4, EU.rng) > 1 and islaz == true then
					pi:Morph(pi.Type, PickupVariant.PICKUP_PILL, 0, true, true, false)
				end
			end
		end
	end)
end