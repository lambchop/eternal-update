EtAPI = RegisterMod("Eternal API", 1)
EtAPI.loaded = false
print("["..EtAPI.Name.."]", "Loading...")

EtAPI.game = Game()
EtAPI.sfx = SFXManager()
EtAPI.rng = RNG()
EtAPI.itempool = EtAPI.game:GetItemPool()

local PATHPREFIX = "scripts/eternalAPI/resources/"

EtAPI:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function(_, save)
	local seed = EtAPI.game:GetSeeds():GetStartSeed()
	EtAPI.rng:SetSeed(seed, 35)
end)

function EtAPI.GetStageName()
	local level = EtAPI.game:GetLevel()
	local stagenum = level:GetStage()
	local stagename = level:GetName()
	if stagenum < 9 then
		if level:GetStage() % 2 ~= 1 or (level:GetCurses() & LevelCurse.CURSE_OF_LABYRINTH > 0) then
			return string.sub(stagename, 0, -4)
		else
			return string.sub(stagename, 0, -3)
		end
	else
		return stagename
	end
end

--[[EtAPI.themes = {
	white = 0,
	normal = 1,
	rbw = 2,
	gay = 3,
	shit = 4
}]]

function EtAPI.len(t)
	local count = 0
	for _ in pairs(t) do count = count + 1 end
	return count
end

function EtAPI.lowest(t) -- returns int
	local v = 99999999
	for i = 1, EtAPI.len(t) do
		if v > t[i] then v = t[i] end
		if i == EtAPI.len(t) then
			return v
		end
	end
end

function EtAPI.highest(t) -- returns int
	local v = 0
	for i = 1, EtAPI.len(t) do
		if v < t[i] then v = t[i] end
		if i == EtAPI.len(t) then
			return v
		end
	end
end

function EtAPI.isSinglePlayer()
	local t = {}
	if t.val == nil then t.val = {} end
	for i = 0, EtAPI.game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		if not t.val[p.ControllerIndex] then t.val[p.ControllerIndex] = true end
		if i == EtAPI.game:GetNumPlayers() - 1 then
			local nump = EtAPI.len(t.val)
			if nump > 1 then
				return false
			else
				return true
			end
			break
		end
	end
end

function EtAPI.getClosestEntity(pos, TYPE, VARIANT, SUBTYPE) -- caps are optional
	local t_dist = {}
	local t = {}
	if TYPE and VARIANT and SUBTYPE then
		t = Isaac.FindByType(TYPE, VARIANT, SUBTYPE, false, false)
	else
		t = Isaac.GetRoomEntities()
	end
	if t ~= nil and EtAPI.len(t) ~= 0 then
		for i, e in pairs(t) do
			t_dist[i] = (pos - e.Position):Length()
			local lowest = EtAPI.lowest(t_dist)
			if t_dist[i] and t_dist[i] == lowest then
				return e
			end
		end
	end
end

function EtAPI.getFurthestEntity(pos, TYPE, VARIANT, SUBTYPE) -- caps are optional
	local t_dist = {}
	local t = {}
	if TYPE and VARIANT and SUBTYPE then
		t = Isaac.FindByType(TYPE, VARIANT, SUBTYPE, false, false)
	else
		t = Isaac.GetRoomEntities()
	end
	if t ~= nil and EtAPI.len(t) ~= 0 then
		for i, e in pairs(t) do
			t_dist[i] = (pos - e.Position):Length()
			local highest = EtAPI.highest(t_dist)
			if t_dist[i] and t_dist[i] == highest then
				return e
			end
		end
	end
end

function EtAPI:getController(p)
	local cidx = p.ControllerIndex
	local pastcoins = p:GetNumCoins()
	local fpid = Game():GetNumPlayers()
	local strP = string.gmatch(Isaac.ExecuteCommand('addplayer '.. PlayerType.PLAYER_THELOST ..' '..cidx),'%S+')
	local w = {}
	for z in strP do
		table.insert(w, z)
	end
	local fp = Isaac.GetPlayer(fpid)
	fp:GetSprite():Load(PATHPREFIX .. 'gfx/player_quickdeath.anm2', false)
	fp.SpriteScale = Vector(0,0)
	fp.ControlsEnabled = false
	fp.Visible = false
	fp.Parent = p
	Game():GetHUD():AssignPlayerHUDs()
	p:AddCoins(-(p:GetNumCoins() - pastcoins))
	fp:Die()
	return w[4]
end

do -- screen functions
	function EtAPI.getScreenCenter()
		local room = Game():GetRoom()
		local shape = room:GetRoomShape()
		local centerOffset = room:GetCenterPos() - room:GetTopLeftPos()
		
		local pos = room:GetCenterPos()
		
		if centerOffset.X > 260 then
			pos.X = pos.X - 260
		end
		if shape == RoomShape.ROOMSHAPE_LBL or shape == RoomShape.ROOMSHAPE_LTL then
			pos.X = pos.X - 260
		end
		if centerOffset.Y > 140 then
			pos.Y = pos.Y - 140
		end
		if shape == RoomShape.ROOMSHAPE_LTR or shape == RoomShape.ROOMSHAPE_LTL then
			pos.Y = pos.Y - 140
		end
		
		return Isaac.WorldToRenderPosition(pos, false)
	end
	
	function EtAPI.getScreenBR()
		local pos = EtAPI.getScreenCenter() * 2
		return pos
	end
	
	function EtAPI.getScreenBL()
		local pos = Vector(0, EtAPI.getScreenBR(0).Y)
		return pos
	end

	function EtAPI.getScreenTR()
		local pos = Vector(EtAPI.getScreenBR(0).X, 0)
		return pos
	end
	
	function EtAPI.getScreenTL()
		local pos = Vector(0, 0)
		return pos
	end
end


function EtAPI.round(num, place)
	local n = (num*place - math.floor(num)*place)/place
	if (n*(place) - math.floor(n*(place))) < 0.5 then
		return math.floor(num*place)/place
	else
		return math.ceil(num*place)/place
	end
end

function EtAPI.unitvector(v)
	return v / math.sqrt(v.X * v.X + v.Y * v.Y)
end

function EtAPI.random(x, y, rng)
	x = EtAPI.round(x, 1)
	y = EtAPI.round(y, 1)
	if not y then
		y = x
		x = 1
	end
	return (rng:RandomInt(y - x + 1)) + x
end

function EtAPI.randomfloat(x, y, rng)
	x = EtAPI.round(x, 1)
	y = EtAPI.round(y, 1)
	if not y then
		y = x
		x = 0
	end
	x = x * 1000
	y = y * 1000
	return math.floor((rng:RandomInt(y - x + 1)) + x) / 1000
end

function EtAPI.getrandomunitvec()
	return EtAPI.unitvector(Vector(EtAPI.randomfloat(-1, 1, EtAPI.rng), EtAPI.randomfloat(-1, 1, EtAPI.rng)))
end

function EtAPI.GetBitList(Array, bitint, eff) -- adds both true and false bits
	local BitList = {}
	for i, e in pairs(Array) do
		if eff then
			if (bitint & e > 0) then
				table.insert(BitList, i)
			end
		else
			if (bitint & e > 0) then
				BitList[i] = true
			else
				BitList[i] = false
			end
		end
	end
	return BitList
end

function EtAPI.GetSoulHearts(p) -- only for soul soul hearts, not black
	local blackheartidx = EtAPI.GetBitList(EtAPI.blackheartarray, p:GetBlackHearts(), true)
	local bhearts = #blackheartidx * 2
	local soulhearts = p:GetSoulHearts() - bhearts
	if soulhearts % 2 == 1 then
		return soulhearts - 1
	else
		return soulhearts 
	end
end

function EtAPI.GetBlackHearts(p) -- returns amount of black hearts
	local blackheartidx = EtAPI.GetBitList(EtAPI.blackheartarray, p:GetBlackHearts(), true)
	local bhearts = #blackheartidx * 2
	local soulhearts = p:GetSoulHearts() - bhearts
	if soulhearts % 2 == 1 then
		return p:GetSoulHearts() - soulhearts - 1
	else
		return p:GetSoulHearts() - soulhearts
	end
end

function EtAPI.GetHeartIndex(p, spec) -- returns heart index, spec is the table of positions of said heart
	local soulhpspace = (math.ceil(p:GetSoulHearts()/2) + p:GetBoneHearts())
	local bhearts = EtAPI.GetBitList(EtAPI.blackheartarray, p:GetBlackHearts(), false)
	
	local nummax = math.ceil(p:GetMaxHearts()/2)
	local numbroke = math.abs(p:GetBrokenHearts())
	
	local brokenspace = nummax + soulhpspace
	
	local idx = {}
	for i = 0, nummax do
		idx[i] = {TYPE = "MAX", SUBTYPE = "", VALUE = "?"}
	end
	for i = 0, soulhpspace do
		if bhearts[i + 1] == false then
			if p:IsBoneHeart(i) then
				idx[i + nummax] = {TYPE = "BONE", SUBTYPE = "", VALUE = "?"}
			else
				if not p:IsBlackHeart(i + 1) then
					idx[i + nummax] = {TYPE = "SOUL", SUBTYPE = "", VALUE = "?"}
				else
					idx[i + nummax] = {TYPE = "SOUL", SUBTYPE = "BLACK", VALUE = "?"}
				end
			end
		end
		if bhearts[i + 1] == true then
			if bhearts[i + 1] == true then
				if p:IsBoneHeart(i) then
					idx[i + nummax] = {TYPE = "BONE", SUBTYPE = "", VALUE = "?"}
				else
					if p:IsBlackHeart(i + 1) then
						idx[i + nummax] = {TYPE = "SOUL", SUBTYPE = "BLACK", VALUE = "?"}
					else
						idx[i + nummax] = {TYPE = "SOUL", SUBTYPE = "", VALUE = "?"}
					end
				end
			end
		end
	end
	for i = 0, numbroke do
		idx[i + brokenspace ] = {TYPE = "BROKEN", SUBTYPE = "", VALUE = ""}
	end
	return idx
end

function EtAPI.GetHeartByIndex(p, idx) -- returns table
	local hidx = EtAPI.GetHeartIndex(p)
	local val = hidx[idx]
	return val
end

--[[function EtAPI.GetHeartIndex(p) -- returns heart index
	local baseidx = EtAPI.GetBitList(EtAPI.blackheartarray, p:GetBlackHearts(), false)
	local bhearts = #EtAPI.GetBitList(EtAPI.blackheartarray, p:GetBlackHearts(), true)
	
	local nummax = math.ceil(p:GetMaxHearts()/2)
	local numbroke = math.abs(p:GetBrokenHearts())
	
	local idx = {}
	
	for i = 1, #baseidx do
		idx[i + nummax] = baseidx[i]
	end
	for i = 1, nummax do
		idx[i] = "red"
	end
	for i = 0, numbroke do
		idx[math.abs(i - 13)] = "broken"
	end
	
	for i = 1, #idx do
		if i > 12 then
			idx[i] = nil
		end
		if idx[i] == true then
			idx[i] = "black"
		end
		if idx[i] == false then
			if p:IsBoneHeart(i) == true then
				idx[i + nummax] = "bone"
			end
		end
	end
	
	return idx
end]]

function printhearts()
	local p = Isaac.GetPlayer()
	local hidx = EtAPI.GetHeartIndex(p)
	for i = 0, #EtAPI.GetHeartIndex(p) - 1 do
		print(EtAPI.GetHeartIndex(p)[i].TYPE, EtAPI.GetHeartIndex(p)[i].SUBTYPE)
	end
end

EtAPI.blackheartarray = {
	[1] = 1<<0,
	[2] = 1<<1,
	[3] = 1<<2,
	[4] = 1<<3,
	[5] = 1<<4,
	[6] = 1<<5,
	--2nd row
	[7] = 1<<6,
	[8] = 1<<7,
	[9] = 1<<8,
	[10] = 1<<9,
	[11] = 1<<10,
	[12] = 1<<11,
}

EtAPI.Colors = {
	COLOR_HOMING = Color(0.4, 0.15, 0.38, 1, 44, 2, 74),
	COLOR_GHOST = Color(0.5, 0.5, 0.5, 0.5, 200, 200, 200)
}

local AchPNG = "None"
local AchPaperPNG = "None"
local AchSND = 0

EtAPI.IsShowingAchievement = false

EtAPI.UiSprite = {
	Ach = Sprite(),
}

EtAPI.UiSprite.Ach:Load(PATHPREFIX .. "gfx/ui/achievements.anm2", true)

local JustShowed = nil

function EtAPI.ForceBloodTear(tear)
	if tear.Variant == TearVariant.BLUE then
		tear:ChangeVariant(TearVariant.BLOOD)
	elseif tear.Variant == TearVariant.NAIL then
		tear:ChangeVariant(TearVariant.NAIL_BLOOD)
	elseif tear.Variant == TearVariant.GLAUCOMA then
		tear:ChangeVariant(TearVariant.GLAUCOMA_BLOOD)
	elseif tear.Variant == TearVariant.CUPID_BLUE then
		tear:ChangeVariant(TearVariant.CUPID_BLOOD)
	elseif tear.Variant == TearVariant.EYE then
		tear:ChangeVariant(TearVariant.EYE_BLOOD)
	elseif tear.Variant == TearVariant.PUPULA then
		tear:ChangeVariant(TearVariant.PUPULA_BLOOD)
	elseif tear.Variant == TearVariant.GODS_FLESH then
		tear:ChangeVariant(TearVariant.GODS_FLESH_BLOOD)
	end
end

function EtAPI.GetScreenCenterPos() -- adopted from piber20helper
	local room = EtAPI.game:GetRoom()
	local shape = room:GetRoomShape()
	local centerOffset = room:GetCenterPos() - room:GetTopLeftPos()
	
	local pos = room:GetCenterPos()
	
	if centerOffset.X > 260 then
		pos.X = pos.X - 260
	end
	if shape == RoomShape.ROOMSHAPE_LBL or shape == RoomShape.ROOMSHAPE_LTL then
		pos.X = pos.X - 260
	end
	if centerOffset.Y > 140 then
		pos.Y = pos.Y - 140
	end
	if shape == RoomShape.ROOMSHAPE_LTR or shape == RoomShape.ROOMSHAPE_LTL then
		pos.Y = pos.Y - 140
	end
	
	return Isaac.WorldToRenderPosition(pos, false)
end

function EtAPI.ShowCustomAchievement(achievementPNG, paperPNG, sound)
	if EtAPI.IsShowingAchievement == false then
		AchPNG = achievementPNG
		AchPaperPNG = paperPNG
		AchSND = sound
		EtAPI.IsShowingAchievement = true
		JustShowed = false
	end
end

EtAPI:AddCallback(ModCallbacks.MC_POST_RENDER, function(_)
	if EtAPI.IsShowingAchievement == true then
		EtAPI.UiSprite.Ach:Update()
		if not JustShowed then
			if type(AchPNG) ~= "string" then AchPNG = PATHPREFIX .. "gfx/ui/achievement_na.png" end
			if type(AchPaperPNG) ~= "string" then AchPaperPNG = PATHPREFIX .. "gfx/ui/paper.png" end
			EtAPI.UiSprite.Ach:ReplaceSpritesheet(0, AchPaperPNG)
			EtAPI.UiSprite.Ach:ReplaceSpritesheet(1, AchPNG)
			EtAPI.UiSprite.Ach:LoadGraphics()
			EtAPI.UiSprite.Ach:Play("Appear", false)
			JustShowed = true
		end
		
		if EtAPI.UiSprite.Ach:GetFrame() == 0 and EtAPI.UiSprite.Ach:IsPlaying("Appear") then
			if type(AchSND) ~= "number" then AchSND = SoundEffect.SOUND_BOOK_PAGE_TURN_12 end
			EtAPI.sfx:Play(AchSND, 1, 0, false, 1)
		end
		
		if EtAPI.UiSprite.Ach:IsFinished("Appear") then
			EtAPI.UiSprite.Ach:Play("Idle", true)
		elseif EtAPI.UiSprite.Ach:IsFinished("Idle") then
			EtAPI.UiSprite.Ach:Play("Disappear", true)
		elseif EtAPI.UiSprite.Ach:IsFinished("Disappear") then
			EtAPI.IsShowingAchievement = false
		end
		
	end
	EtAPI.UiSprite.Ach:Render(Vector(EtAPI.GetScreenCenterPos().X, (EtAPI.GetScreenCenterPos().Y) * 0.4), Vector(0, 0), Vector(0, 0))
end)

EtAPI.loaded = true
print("["..EtAPI.Name.."]", "Finished loading.")