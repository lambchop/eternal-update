if not DEALAPI then DEALAPI = RegisterMod('Deal Api', 1) end
--list of stages which do not have any deals
if not DEALAPI.game then DEALAPI.game = Game() end

local bannedstages = {
	[1] = true,
	[9] = true,
	[10] = true,
	[11] = true,
	[12]=true
}
local blockangel = false
local angelinit = false
local stg = 1
local flag = Game():GetStateFlag(21)			--am abusing fucking flags bc i can!!!!!!
local seendevildoorflag = Game():GetStateFlag(22)	--i hate this game
local seenangeldoorflag = Game():GetStateFlag(23)	--i hate this game

--trying to cache the stage
if Game ~= nil and Game():GetLevel() ~= nil then
	stg = Game():GetLevel():GetStage()
end

local function calcangel()
	local adds = 1
	local fail = 0.5
	local keyfail = 0.75
	local beadfail = 0.5
	local goodbumfail = 0.9
	local badbumfail = 0.9
	local eucharist = false
	local keypieces = {false, false}
	local rosarybead = false
	local goodbum = false
	local badbum = false
	local badbumdied = false
	local dealfail = false
	local hasduality = false
	for pl = 0,Game():GetNumPlayers() - 1 do
		if Isaac.GetPlayer(pl):HasCollectible(CollectibleType.COLLECTIBLE_EUCHARIST) then
			eucharist=true
		end
		if Isaac.GetPlayer(pl):HasCollectible(CollectibleType.COLLECTIBLE_DUALITY) then
			hasduality=true
		end
		if Isaac.GetPlayer(pl):HasCollectible(238) then
			keypieces[1]=true
		end
		if Isaac.GetPlayer(pl):HasCollectible(239) then
			keypieces[2]=true
		end
		if Isaac.GetPlayer(pl):HasTrinket(7) then
			rosarybead=true
		end
	end
	if Game():GetLevel():GetStateFlag(1) then
		badbumdied=true
	end
	if Game():GetLevel():GetStateFlag(3) then
		goodbum=true
	end
	if Game():GetLevel():GetStateFlag(4) then
		badbum=true
	end
	if Game():GetDevilRoomDeals()>0 then 
		dealfail=true
	end
	if eucharist then
		fail=0
	elseif angelinit then
		if dealfail then
			fail=1
		else
			if keypieces[1] then
				adds=adds*keyfail
			end
			if keypieces[2] then
				adds=adds*keyfail
			end
			if rosarybead then
				adds=adds*beadfail
			end
			if goodbum then
				adds=adds*goodbumfail
			end
			if badbumdied then
				adds=adds*keyfail
			end
			if Game():GetDonationModAngel() >= 10 then
				adds=adds*beadfail
			end
			if badbum then
				adds=adds*(1/0.9)
			end
		end
		local extrafail=1-Game():GetLevel():GetAngelRoomChance()
		adds=adds*extrafail
		fail=fail*adds
	else fail=1
	end
	local angel=1-fail
	if angel>1 or (not Game():GetStateFlag(6) and Game():GetStateFlag(5) and not flag) then angel=1 end
	return angel,hasduality
end
function DEALAPI:GetChance()
	local deal=0.0
	local diff=Game().Difficulty
	local room=Game():GetRoom()
	angelinit=Game():GetStateFlag(5)
	local curse=Game():GetLevel():GetCurses()
	if bannedstages[stg]==nil or curse==curse|LevelCurse.CURSE_OF_LABYRINTH or diff>1 then
		deal=room:GetDevilRoomChance()
		if deal>1 then
			deal=1.0
		end
	end
	flag=Game():GetStateFlag(21)
	angel,isduality=calcangel()
	local devil=1-angel
	return deal,angel,devil,isduality
end
function DEALAPI:onlvl()
	stg=Game():GetLevel():GetStage()
end
function DEALAPI:onrender()
	local deal,angel,devil,isduality = DEALAPI:GetChance()
	--Isaac.RenderText('Total Deal Chance: '..(deal*100)..'%',150,75,1,1,1,1)
	--Isaac.RenderText('Devil Chance:      '..(devil*100)..'%',150,75+12,1,1,1,1)
	--Isaac.RenderText('Angel Chance:      '..(angel*100)..'%',150,75+24,1,1,1,1)
end
function DEALAPI:update()
	local room=Game():GetRoom()
	local isboss=room:IsCurrentRoomLastBoss()
	if isboss then
		if room:IsClear() then
			for i=0,7 do
				local grident=room:GetDoor(i)
				if grident and grident:GetType()==16 then
					if grident.TargetRoomType==15 then
						Game():SetStateFlag(21,true)
					end
				end
			end
		end
	end
end
DEALAPI:AddCallback(ModCallbacks.MC_POST_RENDER,DEALAPI.onrender)
DEALAPI:AddCallback(ModCallbacks.MC_POST_UPDATE,DEALAPI.update)
DEALAPI:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL,DEALAPI.onlvl)